/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, {useEffect} from 'react';
import {
  Alert,
  BackHandler,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  useColorScheme,
  View,
} from 'react-native';

import {
  DebugInstructions,
  Header,
  LearnMoreLinks,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';
import AppLogo from './src/components/AppLogo';
import CmButton from './src/components/CmButton';
import RegisterOrLogin from './src/screens/RegisterOrLogin';
import Colors from './src/constants/Colors';
import Register from './src/screens/Register';
import ConfirmRegister from './src/screens/ConfirmRegister';
import Welcome from './src/screens/Welcome';
import Who from './src/screens/questions-screen/Who';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import When from './src/screens/questions-screen/When';
import Where from './src/screens/questions-screen/Where';
import WhatActivies from './src/screens/questions-screen/WhatActivities';
import Loading from './src/components/Loading';
import Dummpy from './src/screens/Dummpy';
import {ThemeProvider} from 'react-native-elements';
import {useTheme} from './src/context/ThemeProvider';
import {Done} from './src/screens/Done';
import {DashBoard} from './src/screens/DashBoard';
import Activities from './src/screens/Activities';
import Chat from './src/screens/Chat';

import {Home} from './src/screens/Home';
import AddCard from './src/screens/payment-card/AddCard';
import PaymentMethod from './src/screens/payment-card/PaymentMethod';
import MoreInfo from './src/screens/NewEvent/MoreInfo';
import NewEvent from './src/screens/NewEvent/NewEvent';
import Profile from './src/screens/Profile';
import Login from './src/screens/Login';
import HalfCalendar from './src/screens/HalfCalendar';
import CreateWhen from './src/screens/NewEvent/CreateWhen';
import CreateWhere from './src/screens/NewEvent/CreateWhere';
import Util from './src/utils/util';
import ChatRoom from './src/screens/ChatRoom';
import Check from './src/screens/Check';
import JoinEvent from './src/screens/NewEvent/JoinEvent';
import JoinedEvent from './src/screens/NewEvent/JoinedEvent';

// import { LogoImagePath } from './src/constants/LogoImagePath';

// const Section: React.FC<{
//   title: string;
// }> = ({children, title}) => {
//   const isDarkMode = useColorScheme() === 'dark';
//   return (
//     <View style={styles.sectionContainer}>
//       <Text
//         style={[
//           styles.sectionTitle,
//           {
//             color: isDarkMode ? Colors.white : Colors.black,
//           },
//         ]}>
//         {title}
//       </Text>
//       <Text
//         style={[
//           styles.sectionDescription,
//           {
//             color: isDarkMode ? Colors.light : Colors.dark,
//           },
//         ]}>
//         {children}
//       </Text>
//     </View>
//   );
// };
const App = () => {
  // const isDarkMode = useColorScheme() === 'dark';

  // const backgroundStyle = {
  //   backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  // };

  const Stack = createNativeStackNavigator();

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', Util.backAction);

    return () =>
      BackHandler.removeEventListener('hardwareBackPress', Util.backAction);
  }, []);

  const theme = useTheme();
  return (
    <ThemeProvider>
      <NavigationContainer>
        <SafeAreaView style={{flex: 1, backgroundColor: theme.backgroundColor}}>
          <StatusBar backgroundColor={Colors.primaryBackground} />
          <Stack.Navigator
            screenOptions={{
              header: () => null,
            }}
            initialRouteName="JoinedEvent">
            <Stack.Screen name="RegisterAndLogin" component={RegisterOrLogin} />
            <Stack.Screen name="Who" component={Who} />
            <Stack.Screen name="Register" component={Register} />
            <Stack.Screen name="Login" component={Login} />
            <Stack.Screen name="ConfirmRegister" component={ConfirmRegister} />
            <Stack.Screen name="Welcome" component={Welcome} />

            <Stack.Screen name="When" component={When} />
            <Stack.Screen name="Where" component={Where} />
            <Stack.Screen name="WhatActivies" component={WhatActivies} />
            <Stack.Screen name="Loading" component={Loading} />
            <Stack.Screen name="Dummpy" component={Dummpy} />
            <Stack.Screen name="DashBoard" component={DashBoard} />
            <Stack.Screen name="Activities" component={Activities} />
            <Stack.Screen name="Chat" component={Chat} />
            <Stack.Screen name="ChatRoom" component={ChatRoom} />
            <Stack.Screen name="Home" component={Home} />
            <Stack.Screen name="Done" component={Done} />
            <Stack.Screen name="Check" component={Check} />
            {/* <Stack.Screen name="Map" component={GooglePlacesInput} /> */}
            <Stack.Screen name="AddCard" component={AddCard} />
            <Stack.Screen name="PaymentMethod" component={PaymentMethod} />
            <Stack.Screen name="NewEvent" component={NewEvent} />
            <Stack.Screen name="MoreInfo" component={MoreInfo} />
            <Stack.Screen name="CreateWhen" component={CreateWhen} />
            <Stack.Screen name="CreateWhere" component={CreateWhere} />
            <Stack.Screen name="JoinEvent" component={JoinEvent} />
            <Stack.Screen name="JoinedEvent" component={JoinedEvent} />
            {/* <Stack.Screen name="Profile" component={Profile} /> */}
          </Stack.Navigator>
        </SafeAreaView>
      </NavigationContainer>
    </ThemeProvider>

    // <SafeAreaView>
    //   <RegisterOrLogin />
    // </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  input: {
    height: 40,
    margin: 12,
    borderWidth: 1,
    padding: 10,
  },
});

export default App;
