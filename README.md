# Boreless

## Description

Boreless Development Project

## Rules

- Backend and Frontend Codes should be in different branch.
- Add merge request if you think bugs are fixed.
- I will check and merge the frontend branch to main branch.
- There should be total three branches 
  - main
  - frontend
  - backend
- There will be tag for each development (initial will start from 0.0.1-alpha)

## Rules for folder structure

- for all the assets including images, logo, fonts etc use assets folder.
- for components use components folder but make sure to give a understandable name to each file in the components.
- If necessary make sub folder in components folder
- For screens use screens folder and sub folders inside it.

## Installation

## Version Dependency

- Node Version : v16.13.0
- NPM Version : 6.14.8
