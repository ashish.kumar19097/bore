import { View, Text, TouchableOpacity } from 'react-native'
import React, { useState } from 'react'
import IconUpArrow from 'react-native-vector-icons/EvilIcons';

interface CancelPoliciesProps {
    text : string
}

const CancelPolicies = ({text} : CancelPoliciesProps) => {
    const [isPoliciesExapnd, setPoliciesExpand] = useState(false);

    const togglePolicies = () => {
        setPoliciesExpand(!isPoliciesExapnd);
    }
    return (
        <>
            <TouchableOpacity onPressIn={() => togglePolicies()} style={{flexDirection: "row", alignItems: "center", width: 150}}>
                <Text style={{fontFamily: "Oxygen-Bold", fontSize: 16, fontWeight: "bold", color: "black"}}>Cancel policies</Text>
                <IconUpArrow name={ isPoliciesExapnd ? "chevron-up" : "chevron-down" } size={35} color={"#6332B3"} style={{fontFamily:"Light"}} />
            </TouchableOpacity>
            {isPoliciesExapnd ?<Text style={{fontFamily: "Oxygen-Regular", fontSize: 16, fontWeight: "400", color: "black", marginTop: 10}}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pellentesque pharetra, mi vitae auctor congue, sem ipsum porta tellus, eget ullamcorper diam turpis ut diam. Curabitur pulvinar mi nec facilisis tristique. Nunc id consequat nisl. Sed enim diam, suscipit viverra consequat sed, tempus a mauris. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae; In convallis orci at viverra lobortis.
            </Text> : <></>}
        </>
    );
}

export default CancelPolicies;