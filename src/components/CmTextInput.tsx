import React from 'react';
import { View, Text, TextInput, ColorValue } from 'react-native';

interface CmTextInputProp {
    placeHolder : string
    placeHolderTextColor: ColorValue
    nameFieldValidator?: (name: string) => void
}


const CmTextInput = ({ placeHolder, placeHolderTextColor, nameFieldValidator } : CmTextInputProp) => {
    return (
        <View style={{marginBottom: 20}}>
            <TextInput
                placeholder= {placeHolder}
                placeholderTextColor={placeHolderTextColor}
                onChangeText={nameFieldValidator}
                style={{
                    backgroundColor: "white", 
                    paddingHorizontal: 20, 
                    fontSize: 18,
                    fontFamily: "Oxygen-Regular",
                    height: 56
                }}
            />
        </View>
            
    )
}

export default CmTextInput;
