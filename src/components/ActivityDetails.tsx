import React, {useState} from 'react';
import {View, Text, ScrollView} from 'react-native';
import {Chip} from 'react-native-elements';
import {Searchbar} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useTheme} from '../context/ThemeProvider';

interface ActivityDetailsProps {
//   onSelect: (value: string) => {};
//   countinueStatus: () => {};
}
const sports = [
  'Cricket',
  'Chess',
  'Hockey',
  'Cards',
  'VolleyBall',
  'Tennis',
  'Ludo',
];
const farming = ['Planting', 'Plucking', 'Watering', 'Spraying', 'Seeding'];

const ActivityDetails = ({}: ActivityDetailsProps) => {
  const [sportsFilterd, setSportsFilterd] = useState(sports);
  const [farmingFilterd, setFarmingFilterd] = useState(farming);
  const [activityName, setActivityName] = useState(String);
  const [countinueStatus, setCountinueStatus] = useState(false);
  const [showAcivitis, setShowActivities] = useState(true);
  const theme = useTheme();
  return (
    <ScrollView style={{height: 250, marginTop: 10}}>
      {sportsFilterd.length > 0 ? (
        <Text
          style={{
            flex: 1,
            justifyContent: 'flex-start',
            fontSize: 18,
            fontWeight: '400',
            color: theme.primaryTextColor,
            paddingBottom: 10,
            paddingTop: 15,
          }}>
          Sports
        </Text>
      ) : null}
      <View
        style={{
          borderRadius: 4,
          borderColor: 'black',
          flexDirection: 'row',
          flexWrap: 'wrap',
        }}>
        {sportsFilterd.map((text, index) => (
          <Chip
            title={text}
            key={index}
            type="outline"
            titleStyle={{
              fontSize: 18,
              color: 'black',
              fontWeight: 'bold',
            }}
            onPress={() => {
              setActivityName(text);
              setShowActivities(false);
            }}
            buttonStyle={{borderWidth: 2, borderColor: 'black'}}
            containerStyle={{
              marginVertical: 5,
              paddingLeft: 5,
              paddingRight: 5,
            }}
          />
        ))}
      </View>

      {farmingFilterd.length > 0 ? (
        <Text
          style={{
            flex: 1,
            justifyContent: 'flex-start',
            fontSize: 18,
            fontWeight: '400',
            color: theme.primaryTextColor,
            paddingBottom: 10,
            paddingTop: 15,
          }}>
          Farming
        </Text>
      ) : null}
      <View
        style={{
          borderRadius: 4,
          borderColor: 'black',
          flexDirection: 'row',
          flexWrap: 'wrap',
        }}>
        {farmingFilterd.map((text, index) => (
          <Chip
            key={index}
            title={text}
            type="outline"
            onPress={() => {
              setActivityName(text);
              setShowActivities(false);
            }}
            buttonStyle={{borderWidth: 2, borderColor: 'black'}}
            titleStyle={{
              fontSize: 18,
              color: 'black',
              fontWeight: 'bold',
            }}
            containerStyle={{
              marginVertical: 5,
              paddingLeft: 5,
              paddingRight: 5,
            }}
          />
        ))}
      </View>

      {sportsFilterd.length == 0 && farmingFilterd.length == 0 && (
        <Text
          style={{
            flex: 1,
            justifyContent: 'flex-start',
            fontSize: 18,
            color: theme.primaryTextColor,
            paddingBottom: 10,
            paddingTop: 15,
          }}>
          No search found
        </Text>
      )}
    </ScrollView>
  );
};

export default ActivityDetails;
