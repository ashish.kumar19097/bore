import React from 'react';
import {ColorValue, StyleProp, TextStyle, View, ViewStyle} from 'react-native';
import {Divider} from 'react-native-elements';
import {FAB, Text} from 'react-native-paper';
import {useTheme} from '../context/ThemeProvider';
import CmButton from './CmButton';
interface ProfileStatusProps {
  label1: string;
  lable2: string;
  lable3?: string;
  goClick: () => void;
}

const ProfileStatus = ({
  label1,
  lable2,
  lable3,
  goClick,
}: ProfileStatusProps) => {
  const theme = useTheme();
  return (
    <View style={{marginHorizontal: 60}}>
      <View
        style={{
          display: 'flex',

          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <FAB
          small={true}
          icon={'check-bold'}
          style={{backgroundColor: 'black', width: 40, height: 40}}></FAB>
        <Text
          style={{
            fontSize: 18,
            marginHorizontal: 20,
            fontWeight: '700',
            fontFamily: 'Oxygen-Bold',
          }}>
          {label1}
        </Text>
      </View>
      <View
        style={{
          height: 40,
          width: 2,
          marginHorizontal: 20,
          backgroundColor: 'black',
        }}></View>
      <View
        style={{
          display: 'flex',

          flexDirection: 'row',
          alignItems: 'center',
        }}>
        <FAB
          small={true}
          icon={'numeric-2'}
          style={{backgroundColor: 'black', width: 40, height: 40}}></FAB>
        <Text
          style={{
            fontSize: 18,
            marginHorizontal: 20,
            fontWeight: '700',
            fontFamily: 'Oxygen-Bold',
          }}>
          {lable2}
        </Text>
      </View>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
        }}>
        {lable3 != null ? (
          <View>
            <View
              style={{
                height: 30,
                width: 2,

                marginLeft: 20,
                backgroundColor: 'black',
              }}></View>
            <View
              style={{
                height: 100,
                width: 2,

                marginLeft: 20,
                backgroundColor: theme.buttonColors.primary.inActive,
              }}></View>
            <View
              style={{
                display: 'flex',

                flexDirection: 'row',
                alignItems: 'center',
              }}>
              <FAB
                small={true}
                icon={'numeric-3'}
                style={{
                  backgroundColor: theme.buttonColors.primary.inActive,
                  width: 40,
                  height: 40,
                }}></FAB>
            </View>
          </View>
        ) : null}
        <View style={{marginLeft: lable3 != null ? 20 : 60}}>
          <Text>You need </Text>
          <CmButton
            label={'Go'}
            buttonStyle={{
              backgroundColor: theme.buttonColors.primary.active,
              height: 35,
              marginTop: 20,
              width: 200,
            }}
            buttonFocusedColor={theme.buttonColors.primary.focused}
            buttonLabelStyle={{
              color: 'white',
            }}
            onClick={() => goClick()}
          />
          {lable3 != null ? (
            <Text
              style={{
                fontSize: 18,
                marginTop: 60,
                fontWeight: '700',
                fontFamily: 'Oxygen-Bold',
                color: theme.buttonColors.primary.inActive,
              }}>
              {lable3}
            </Text>
          ) : null}
        </View>
      </View>
    </View>
  );
};
export default ProfileStatus;
