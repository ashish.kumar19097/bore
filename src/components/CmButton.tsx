import React from 'react';
import { View, Button, StyleSheet, TouchableHighlight, Text, StyleProp, ViewStyle, ColorValue, TextStyle } from 'react-native';
import { useTheme } from '../context/ThemeProvider';

interface CmButtonProp {
    label : string,
    buttonStyle : StyleProp<ViewStyle>,
    buttonFocusedColor: ColorValue,
    buttonLabelStyle: StyleProp<TextStyle>
    onClick: () => any
}

const CmButton = ({ label, buttonStyle, buttonFocusedColor, buttonLabelStyle, onClick} : CmButtonProp) => {
 
 
    return (
        <TouchableHighlight underlayColor={buttonFocusedColor} 
            onPress={() => onClick()}
            style={[buttonStyle, {}]}
        >
            <Text
                style={[buttonLabelStyle, {
                    padding: 10,
                    textAlign: "center"
                }]}
            >{label}</Text>
        </TouchableHighlight>
    );
}

export default CmButton;



