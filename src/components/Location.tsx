import {View, Text, Image} from 'react-native';
import React from 'react';
const Location = () => {
  return (
    <View style={{marginTop: 15, height: 280}}>
      <Image
        source={require('../../assets/images/map.png')}
        style={{
          resizeMode: 'cover',
          flex: 1,
          width: '100%',
          height: '100%',
        }}></Image>
      <Text
        style={{
          fontFamily: 'Oxygen-Regular',
          fontWeight: '400',
          fontSize: 16,
          color: 'black',
          marginHorizontal: 15,
          marginTop: 20,
        }}>
        Address, address, address, address, 0000000
      </Text>
    </View>
  );
};

export default Location;
