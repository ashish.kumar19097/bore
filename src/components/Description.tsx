import React from 'react';
import { StyleProp, StyleSheet, Text, TextStyle, View, ViewStyle } from 'react-native';
import { useTheme } from '../context/ThemeProvider';

interface DescriptionProps {
    text: string,
    Textstyle? : StyleProp<TextStyle>
}

export const Description  = ({ text, Textstyle } : DescriptionProps) => {
    const theme = useTheme();
    return (
        <Text 
            style={[
                Textstyle,
                {
                    fontSize: 16, 
                    lineHeight: 28, 
                    color: theme.primaryTextColor,
                    fontFamily: "Oxygen-Regular"
                }
            ]}
        >
            {text}
        </Text>
    );
}


