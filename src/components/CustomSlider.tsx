import React, { Component, useState } from 'react'
import { Dimensions, PointPropType, StyleSheet, Text, View } from 'react-native';
import { Slider } from 'react-native-elements/dist/slider/Slider';
import { useTheme } from '../context/ThemeProvider';

const CustomSlider = () => {

    const [value, setValue] = useState(1)
    const theme = useTheme()
    return (
        <View style={{marginHorizontal: 20}}>
            <View style={{position: "relative", top: 30 ,backgroundColor: "#894CED", height:20, width: 20, borderRadius: 50}}></View>
            <Slider
                value={value}
                onValueChange={value => {
                setValue(value);
                }}
                minimumTrackTintColor='#894CED'
                maximumValue={50}
                minimumValue={1}
                step={1}
                allowTouchTrack
                trackStyle={{
                height: 2,
                backgroundColor: theme.buttonColors.primary.active,
                }}
                thumbStyle={{
                height: 30,
                width: 30,
                backgroundColor: theme.buttonColors.primary.active,
                }}

                thumbProps={{
                    children: (
                        <View style={{bottom: 70, right: 40}}>
                        <Text
                            style={{
                            color: 'white',
                            fontSize: 18,
                            height: 48,
                            width: 104,
                            borderRadius: 25,
                            backgroundColor: theme.buttonColors.primary.active,
                            textAlign: 'center',
                            textAlignVertical: 'center',
                            }}>
                            {value}Km
                        </Text>
                        <View
                            style={{
                            height: 30,
                            width: 4,
                            left: 52,
                            backgroundColor: theme.buttonColors.primary.active,
                            }}></View>
                        </View>
                    ),
                }}
            />
        </View>
    );
}

export default CustomSlider;

var styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center'
    },
});
