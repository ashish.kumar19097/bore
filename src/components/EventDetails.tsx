import React from 'react';
import {Text, View} from 'react-native';
import {Image} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {useTheme} from '../context/ThemeProvider';
interface EventDetailsProps {
  eventDetails: {
    id: number;
    eventName: string;
    image: any;
    level: string;
    totalMember: string;
    currentEnroll: string;
    date: string;
    place: string;
    distance: string;
    charges: string;
  };
}

const EventDetails = ({eventDetails}: EventDetailsProps) => {
  const theme = useTheme();
  return (
    <View
      key={eventDetails.id}
      style={{flex: 1, height: 200, marginHorizontal: 24}}>
      <Image
        style={{
          resizeMode: 'cover',
          flex: 1,
          width: '100%',
          height: '100%',
        }}
        source={eventDetails.image}
      />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 8,
        }}>
        <Text style={{fontSize: 16, fontWeight: '600', color: 'black'}}>
          {eventDetails.eventName}
        </Text>
        <Text style={{fontSize: 14, fontWeight: '600', color: 'black'}}>
          {eventDetails.date}
        </Text>
      </View>
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginBottom: 10,
        }}>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Icon
            name="chart-bell-curve-cumulative"
            size={18}
            style={{
              color: theme.buttonColors.primary.active,
              marginRight: 5,
              alignSelf: 'center',
            }}
          />
          <Text style={{fontSize: 14, fontWeight: '600', color: 'black'}}>
            {eventDetails.level}
          </Text>
          <Icon
            name="account-multiple-outline"
            size={18}
            style={{
              color: theme.buttonColors.primary.active,
              marginHorizontal: 6,
              alignSelf: 'center',
            }}
          />
          <Text style={{fontSize: 14, fontWeight: '600', color: 'black'}}>
            {eventDetails.currentEnroll}/{eventDetails.totalMember}
          </Text>
          <Icon
            name="currency-jpy"
            size={18}
            style={{
              color: theme.buttonColors.primary.active,
              marginHorizontal: 6,
              alignSelf: 'center',
            }}
          />
          <Text style={{fontSize: 14, fontWeight: '600', color: 'black'}}>
            {eventDetails.charges}
          </Text>
        </View>
        <View
          style={{
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}>
          <Icon
            name="map-marker-outline"
            size={18}
            style={{
              color: theme.buttonColors.primary.active,
              marginHorizontal: 6,
              alignSelf: 'center',
            }}
          />
          <Text style={{fontSize: 14, fontWeight: '600', color: 'black'}}>
            {eventDetails.place},{eventDetails.distance}
          </Text>
        </View>
      </View>
    </View>
  );
};

export default EventDetails;
