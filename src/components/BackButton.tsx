import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  ColorValue,
  StyleProp,
  ViewStyle,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

interface BackButtonProps {
  buttonColor: ColorValue;
  iconColor: ColorValue;
  buttonStyle?: StyleProp<ViewStyle>;
  onButtonClick: () => void;
  icon?: string;
}

const BackButton = ({
  buttonColor,
  iconColor,
  buttonStyle,
  onButtonClick,
  icon,
}: BackButtonProps) => {
  return (
    <TouchableOpacity
      activeOpacity={1}
      onPress={() => onButtonClick()}
      style={[
        buttonStyle,
        {
          width: 48,
          height: 48,
          backgroundColor: buttonColor,
          borderRadius: 50,
          justifyContent: 'center',
          alignItems: 'center',
        },
      ]}>
      <Icon
        name={icon == null ? 'arrow-left' : icon}
        size={25}
        color={iconColor}
        style={{fontFamily: 'Light'}}
      />
    </TouchableOpacity>
  );
};

export default BackButton;
