import React from 'react';
import {Dimensions} from 'react-native';
import {Button, Dialog, Text} from 'react-native-paper';
import {useTheme} from '../context/ThemeProvider';

interface CmPopUpProps {
  title: string;
  body: string;
  yesButtonText: string;
  cancelButtonText: string;
  onYesButtonClick: () => void;
  onCancelButtonClick: () => void;
}

const CmPopUp = ({
  title,
  body,
  yesButtonText,
  cancelButtonText,
  onCancelButtonClick,
  onYesButtonClick,
}: CmPopUpProps) => {
  const theme = useTheme();
  return (
    <Dialog visible={true} onDismiss={() => onCancelButtonClick()}>
      <Dialog.Title
        style={{fontSize: 30, fontFamily: 'Oxygen', fontWeight: 'bold'}}>
        {title}
      </Dialog.Title>
      <Dialog.Content>
        <Text style={{fontSize: 20}}>{body}</Text>
      </Dialog.Content>

      <Dialog.Actions style={{justifyContent: 'space-evenly'}}>
        <Button
          mode="contained"
          uppercase={false}
          onPress={() => onYesButtonClick()}
          style={{
            backgroundColor: theme.backButton.iconColor,
            width: Dimensions.get('window').width * 0.35,
          }}>
          {yesButtonText}
        </Button>
        <Button
          uppercase={false}
          mode="outlined"
          onPress={() => onCancelButtonClick()}
          style={{
            borderColor: theme.backButton.iconColor,
            borderWidth: 2,
            width: Dimensions.get('window').width * 0.35,
          }}>
          {cancelButtonText}
        </Button>
      </Dialog.Actions>
    </Dialog>
  );
};

export default CmPopUp;
