import { View, Text } from 'react-native';
import React from 'react';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconGraph from 'react-native-vector-icons/Octicons';
import IconUsers from 'react-native-vector-icons/Feather';
import IconYen from 'react-native-vector-icons/FontAwesome';
import IconLocation from 'react-native-vector-icons/Octicons';
import IconUpArrow from 'react-native-vector-icons/EvilIcons';

const EventBriefDetails = () => {
    return (
        <View style={{ padding: 25 }}>
            <View style={{flexDirection: "row"}}>
                <Icon name="event-available" size={25} color={"#6332B3"} style={{fontFamily:"Light"}}/>
                <Text style={{fontFamily: "Oxygen-Regular", fontWeight: "400", fontSize: 24, color: "black", marginLeft: 15}}>12:00-14:00  11/6 (Sat)</Text>
            </View>
            <View style={{flexDirection: "row"}}>
                <IconGraph name="graph" size={25} color={"#6332B3"} style={{fontFamily:"Light"}}/>
                <Text style={{fontFamily: "Oxygen-Regular", fontWeight: "400", fontSize: 24, color: "black", marginLeft: 15}}>Intermediate</Text>
            </View>
            <View style={{flexDirection: "row"}}>
                <View style={{flexDirection: "row", alignItems: "center"}}>
                    <IconUsers name="users" size={25} color={"#6332B3"} style={{fontFamily:"Light"}}/>
                    <Text style={{fontFamily: "Oxygen-Regular", fontWeight: "400", fontSize: 24, color: "black", marginLeft: 15}}>5/12</Text>
                </View>
                <View style={{flexDirection: "row", marginLeft: 20, alignItems: "center"}}>
                    <IconYen name="yen" size={25} color={"#6332B3"} style={{fontFamily:"Light"}}/>
                    <Text style={{fontFamily: "Oxygen-Regular", fontWeight: "400", fontSize: 24, color: "black", marginLeft: 10}}>500</Text>
                </View>
            <View style={{flexDirection: "row",marginLeft: 20, alignItems: "center"}}>
                <IconLocation name="location" size={25} color={"#6332B3"} style={{fontFamily:"Light"}}/>
                <Text style={{fontFamily: "Oxygen-Regular", fontWeight: "400", fontSize: 24, color: "black", marginLeft: 10}}>13km</Text>
            </View>
        </View>
      </View>
  );
}

export default EventBriefDetails;