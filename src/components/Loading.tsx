import React from 'react';
import {StyleSheet, View} from 'react-native';
import Colors from '../constants/Colors';
import LogoImagePath from '../constants/LogoImagePath';
import {useTheme} from '../context/ThemeProvider';
import AppLogo from './AppLogo';

const Loading = ({navigation}:any) => {
  const theme = useTheme();
  setTimeout(() => {navigation.navigate('Done')},1000);
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: theme.backgroundColor,
        alignItems: 'center',
        justifyContent: 'center',
      }}>
      <AppLogo
        bigHalfLetter={LogoImagePath.GreenColor.bigHalfLetter}
        smallHalfLetter={LogoImagePath.GreenColor.smallHalfLetter}
        words={null}
      />
    </View>
  );
};

export default Loading;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.primaryBackground,
  },
});
