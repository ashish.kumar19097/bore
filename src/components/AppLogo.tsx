import React from 'react'
import { Image, StyleSheet, View } from 'react-native'

interface AppLogoProps {
    bigHalfLetter : any,
    smallHalfLetter : any,
    words? : any
};

const AppLogo = ({ bigHalfLetter, smallHalfLetter, words = undefined }: AppLogoProps) =>  {
    return (
        <View style={styles.container}>
            <View style={styles.letterContainer}>
                    <Image source={bigHalfLetter}></Image>
                    <Image style={styles.smallHalfB} source={smallHalfLetter}></Image>
            </View>
            {words != undefined ? <Image style={styles.word} source={words}></Image>: <View></View>}
        </View>
    );
}

export default AppLogo;

const styles = StyleSheet.create({
    container : {
        width: 80,
        flexDirection: "row",
    },

    smallHalfB : {
        position: "relative",
        top: -8
    },

    word : {
        marginLeft: 6
    },

    letterContainer : {
        alignItems: "flex-end"
    },

});
