import moment from 'moment';
import React, {Component} from 'react';
import {
  Button,
  ScrollView,
  StyleProp,
  Text,
  TextStyle,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import SelectDropdown from 'react-native-select-dropdown';
interface CalenderProps {
  calenderData: Function;
  fullCalenar?: boolean;
  headerStyle?: StyleProp<TextStyle>;
}

class Calender extends Component<CalenderProps> {
  constructor(props: any) {
    super(props);
  }
  months = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
  ];

  weekDays = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

  nDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  state = {
    activeDate: new Date(),
    dateDropDown: new Date(),
    halfCalender:
      this.props.fullCalenar != null ? this.props.fullCalenar : true,
    currentIndex: 0,
  };

  monthDropDown() {
    var months = [];
    this.state.dateDropDown = new Date();
    months.push(
      this.months[this.state.dateDropDown.getMonth()] +
        ' ' +
        this.state.dateDropDown.getFullYear(),
    );
    for (let month = 0; month < 36; month++) {
      this.state.dateDropDown.setMonth(this.state.dateDropDown.getMonth() + 1);
      months.push(
        this.months[this.state.dateDropDown.getMonth()] +
          ' ' +
          this.state.dateDropDown.getFullYear(),
      );
    }
    return months;
  }

  render() {
    var rows = [];

    var matrix = this.generateMatrix();
    return (
      <ScrollView style={{backgroundColor: '#F2F4FE'}}>
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-between',
          }}>
          <SelectDropdown
            buttonStyle={{
              backgroundColor: '#F2F4FE',
              width: 230,
              marginLeft: 0,
            }}
            buttonTextStyle={
              this.props.headerStyle != null
                ? this.props.headerStyle
                : {
                    fontWeight: 'bold',
                    fontSize: 23,
                    color: '#894CED',
                  }
            }
            dropdownBackgroundColor="#F2F4FE"
            renderDropdownIcon={() => (
              <Icon
                name="chevron-down"
                size={25}
                style={{
                  fontFamily: 'Light',
                }}
              />
            )}
            dropdownStyle={{
              borderWidth: 2,
              borderRadius: 10,
              overflow: 'hidden',
              borderColor: 'white',
            }}
            defaultButtonText={
              this.months[this.state.activeDate.getMonth()] +
              ' ' +
              this.state.activeDate.getFullYear()
            }
            data={this.monthDropDown()}
            onSelect={(selectedItem, index) => {
              this.setState(() => {
                if (index < this.state.currentIndex) {
                  this.state.activeDate.setMonth(
                    this.state.activeDate.getMonth() -
                      (this.state.currentIndex - index),
                  );
                  this.state.currentIndex;
                  //this.setState({currentIndex: index});
                } else if (index > this.state.currentIndex) {
                  this.state.activeDate.setMonth(
                    this.state.activeDate.getMonth() +
                      (index - this.state.currentIndex),
                  );
                  //this.setState({currentIndex: index});
                }

                //return this.state;
              });
              this.setState({currentIndex: index});
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              return item;
            }}
          />
          <Icon
            name={
              this.state.halfCalender ? 'arrow-collapse-all' : 'arrow-expand'
            }
            size={25}
            style={{fontFamily: 'Light', color: '#894CED', paddingRight: 20}}
            onPress={() => {
              this.state.halfCalender
                ? this.setState({halfCalender: false})
                : this.setState({halfCalender: true});

              this.props.calenderData({
                date: null,
                half: this.state.halfCalender,
              });
            }}
          />
        </View>

        {
          (rows = matrix.map((row, rowIndex) => {
            var rowItems = row.map((item: any, colIndex: any) => {
              return item != this.state.activeDate.getDate().toString() ? (
                <Text
                  key={colIndex}
                  style={{
                    flex: 1,
                    height: 20,
                    textAlign: 'center',
                    backgroundColor: '#F2F4FE',
                    color: '#000',
                    fontWeight: rowIndex == 0 ? 'bold' : 'normal',
                  }}
                  onPress={() => {
                    this.dateSelect(item);
                    this.props.calenderData({
                      date: this.state.activeDate,
                      half: true,
                    });
                  }}>
                  {parseInt(item) != -1 ? item : ''}
                </Text>
              ) : (
                <View
                  key={colIndex}
                  style={{
                    borderRadius: 20,
                    width: 40,
                    height: 40,
                    backgroundColor: '#894CED',
                    padding: 10,
                    alignItems: 'center',
                  }}>
                  <Text
                    style={{color: 'white', fontWeight: 'bold'}}
                    onPress={() => {
                      this.dateSelect(item);
                      this.props.calenderData({
                        date: this.state.activeDate,
                        half: true,
                      });
                    }}>
                    {' '}
                    {parseInt(item) != -1 ? item : ''}
                  </Text>
                </View>
              );
            });
            return (
              <View
                key={rowIndex}
                style={{
                  flex: 1,
                  flexDirection: 'row',
                  padding: 15,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                {rowItems}
              </View>
            );
          }))
        }
      </ScrollView>
    );
  }
  dateSelect = (item: any) => {
    this.setState(() => {
      if (!item.match && item != -1) {
        this.state.activeDate.setDate(item);
      }
    });
    this.setState({halfCalender: false});
    return this.state;
  };

  generateMatrix() {
    var matrix = [];
    var halfMatrix = [];
    halfMatrix[0] = this.weekDays;
    matrix[0] = this.weekDays;
    var year = this.state.activeDate.getFullYear();
    var month = this.state.activeDate.getMonth();

    var firstDay = new Date(year, month, 1).getDay();
    var maxDays = this.nDays[month];
    if (month == 1) {
      if ((year % 4 == 0 && year % 100 != 0) || year % 400 == 0) {
        maxDays += 1;
      }
    }

    var counter = 1;
    for (var row = 1; row < 7; row++) {
      matrix[row] = [];
      for (var col = 0; col < 7; col++) {
        matrix[row][col] = -1;
        if (row == 1 && col >= firstDay) {
          matrix[row][col] = counter++;
        } else if (row > 1 && counter <= maxDays) {
          matrix[row][col] = counter++;
        }
      }
    }

    if (this.state.halfCalender) {
      return matrix;
    } else {
      for (var i = 0; i < matrix.length; i++) {
        for (var j = 0; j < matrix[i].length; j++) {
          if (matrix[i][j] == this.state.activeDate.getDate().toString()) {
            halfMatrix[1] = matrix[i];
          }
        }
      }
      return halfMatrix;
    }
  }
}

export default Calender;
