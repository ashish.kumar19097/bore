import React, { createContext, useContext, useState } from 'react'
import { View, Text, useColorScheme } from 'react-native'
import { DarkTheme, DefaultTheme } from '../constants/theme';

const ThemeContext = createContext(DefaultTheme);
const ThemeProvider = ({ children } : any) => {
    const isDarkMode = useColorScheme() === 'dark';
    const [theme, setTheme] = useState(DefaultTheme)
    return (
        <ThemeContext.Provider value={isDarkMode ? DarkTheme : DefaultTheme}>
            {children}
        </ThemeContext.Provider>
    )
}

export const useTheme = () => useContext(ThemeContext)
