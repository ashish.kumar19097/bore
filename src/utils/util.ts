import {Component} from 'react';
import { Alert, BackHandler } from 'react-native';

class Util extends Component {
   public static backAction = () => {
        Alert.alert("Hold on!", "Are you sure you want to go back?", [
          {
            text: "Cancel",
            onPress: () => null,
            style: "cancel"
          },
          { text: "YES", onPress: () => BackHandler.exitApp() }
        ]);
        return true;
      };
    
}
export default Util;
