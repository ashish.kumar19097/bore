import {View, Text, ScrollView, Button, Dimensions} from 'react-native';
import React, {useState} from 'react';
import BackButton from '../../components/BackButton';
import {useTheme} from '../../context/ThemeProvider';
import {Appbar, Dialog} from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import IconGraph from 'react-native-vector-icons/Octicons';
import IconUsers from 'react-native-vector-icons/Feather';
import IconYen from 'react-native-vector-icons/FontAwesome';
import IconLocation from 'react-native-vector-icons/Octicons';
import IconUpArrow from 'react-native-vector-icons/EvilIcons';
import Location from '../../components/Location';
import CmButton from '../../components/CmButton';
import CancelPolicies from '../../components/CancelPolicies';

const JoinEvent = () => {
  const [visible, setVisible] = React.useState(false);

  const showDialog = () => setVisible(true);
  const hideDialog = () => setVisible(false);

  const theme = useTheme();
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
        flexDirection: 'column',
        justifyContent: 'flex-end',
      }}>
      <View
        style={{
          backgroundColor: '#050B31',
          padding: 10,
          justifyContent: 'center',
        }}>
        <BackButton
          buttonStyle={{marginLeft: 10}}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
          onButtonClick={() => {}}
        />
        <Text
          style={{
            marginLeft: 0,
            position: 'absolute',
            left: '47%',
            fontSize: 18,
            color: 'white',
          }}>
          {' '}
          Event..
        </Text>
      </View>
      {/* <Appbar.Header style={{backgroundColor: "#050B31"}} statusBarHeight={10}>
                <BackButton buttonStyle={{marginLeft: 10}} buttonColor={theme.backButton.backgroundColor} iconColor={theme.backButton.iconColor} onButtonClick={()=> {}} />
                <Appbar.Content style={{marginLeft: 0, position: "absolute", left: 0, right: 0}}  titleStyle={{ alignSelf: 'center' }} title="Event.." />
            </Appbar.Header> */}

      <View style={{backgroundColor: '#050B31', padding: 20}}>
        <View style={{flexDirection: 'row', alignItems: 'center'}}>
          <Icon
            name="event-available"
            size={20}
            color={'#55BADE'}
            style={{fontFamily: 'Light'}}
          />
          <Text
            style={{
              fontFamily: 'Oxygen-Regular',
              fontWeight: '400',
              fontSize: 16,
              color: 'white',
              marginLeft: 7,
            }}>
            12:00-14:00 11/6 (Sat)
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: 20,
              alignItems: 'center',
            }}>
            <IconGraph
              name="graph"
              size={20}
              color={'#55BADE'}
              style={{fontFamily: 'Light'}}
            />
            <Text
              style={{
                fontFamily: 'Oxygen-Regular',
                fontWeight: '400',
                fontSize: 16,
                color: 'white',
                marginLeft: 7,
              }}>
              Intermediate
            </Text>
          </View>
        </View>
        <View style={{flexDirection: 'row', marginTop: 15}}>
          <IconUsers
            name="users"
            size={20}
            color={'#55BADE'}
            style={{fontFamily: 'Light'}}
          />
          <Text
            style={{
              fontFamily: 'Oxygen-Regular',
              fontWeight: '400',
              fontSize: 16,
              color: 'white',
              marginLeft: 10,
            }}>
            5/12
          </Text>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: 30,
              alignItems: 'center',
            }}>
            <IconYen
              name="yen"
              size={20}
              color={'#55BADE'}
              style={{fontFamily: 'Light'}}
            />
            <Text
              style={{
                fontFamily: 'Oxygen-Regular',
                fontWeight: '400',
                fontSize: 16,
                color: 'white',
                marginLeft: 10,
              }}>
              500
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row',
              marginLeft: 30,
              alignItems: 'center',
            }}>
            <IconLocation
              name="location"
              size={20}
              color={'#55BADE'}
              style={{fontFamily: 'Light'}}
            />
            <Text
              style={{
                fontFamily: 'Oxygen-Regular',
                fontWeight: '400',
                fontSize: 16,
                color: 'white',
                marginLeft: 10,
              }}>
              13km
            </Text>
          </View>
        </View>
      </View>
      <ScrollView>
        <View style={{marginHorizontal: 20}}>
          <Location />
        </View>
        <CmButton
          label={'Show route'}
          buttonStyle={{
            borderColor: theme.buttonColors.primary.active,
            borderWidth: 2,
            marginHorizontal: 60,
            marginTop: 20,
          }}
          buttonFocusedColor={''}
          buttonLabelStyle={{
            fontFamily: 'Oxygen-Regular',
            fontWeight: 'bold',
            fontSize: 14,
            color: theme.ActionTextColor,
          }}
          onClick={function () {
            throw new Error('Function not implemented.');
          }}
        />
        <View style={{marginHorizontal: 20, marginTop: 20}}>
          <CancelPolicies text={''} />
        </View>
        <View style={{height: 120}}></View>
      </ScrollView>

      <View
        style={{
          width: '100%',
          height: 100,
          position: 'absolute',
          bottom: 0,
          left: 0,
          justifyContent: 'center',
          backgroundColor: 'white',
          shadowColor: 'black',
          shadowOffset: {width: 0, height: 2},
          shadowOpacity: 0.25,
          shadowRadius: 4,
          elevation: 0,
        }}>
        <CmButton
          label={'Show route'}
          buttonStyle={{
            backgroundColor: theme.buttonColors.primary.active,
            marginHorizontal: 40,
          }}
          buttonFocusedColor={''}
          buttonLabelStyle={{
            fontFamily: 'Oxygen-Regular',
            fontWeight: 'bold',
            fontSize: 14,
            color: 'white',
          }}
          onClick={() => showDialog()}
        />
      </View>
      <Dialog style={{bottom: 20}} visible={visible} onDismiss={hideDialog}>
        <Dialog.Title
          style={{
            fontSize: 28,
            fontFamily: 'Oxygen-Bold',
            fontWeight: 'bold',
            color: '#000000',
          }}>
          Join the event
        </Dialog.Title>
        <Dialog.Content>
          <Text
            style={{
              fontSize: 16,
              fontFamily: 'Oxygen-Regular',
              color: '#000000',
            }}>
            Proin ut dui sed metus pharetra hend rerit vel non mi. Nulla ornare
            faucibus ex, non facilisis nisl. Maecenas aliquet mauris ut tempus
          </Text>
        </Dialog.Content>
        <Dialog.Actions style={{width: '100%', padding: 10}}>
          <CmButton
            label={'Understood'}
            buttonStyle={{
              backgroundColor: theme.buttonColors.primary.active,
              flex: 1,
            }}
            buttonFocusedColor={''}
            buttonLabelStyle={{
              color: 'white',
              fontFamily: 'Oxygen-Bold',
              fontSize: 14,
            }}
            onClick={function () {
              throw new Error('Function not implemented.');
            }}
          />
          <View style={{marginHorizontal: 10}}></View>
          <CmButton
            label={'No'}
            buttonStyle={{
              borderWidth: 1,
              borderColor: theme.buttonColors.outline.active,
              flex: 1,
            }}
            buttonFocusedColor={''}
            buttonLabelStyle={{fontFamily: 'Oxygen-Bold', fontSize: 14}}
            onClick={function () {
              throw new Error('Function not implemented.');
            }}
          />
        </Dialog.Actions>
      </Dialog>
    </View>
  );
};

export default JoinEvent;
