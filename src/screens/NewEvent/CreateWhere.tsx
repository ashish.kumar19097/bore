import React, {useState} from 'react';
import {Image, ScrollView, View} from 'react-native';
import {Button, Searchbar, Text, TextInput} from 'react-native-paper';
import BackButton from '../../components/BackButton';
import CmButton from '../../components/CmButton';
import {useTheme} from '../../context/ThemeProvider';

function CreateWhere({navigation}:any) {
  const theme = useTheme();
  const [searcAddress, setSearchAddress] = useState(String);
  const [searchComplete, setSearchComplete] = useState(false);
  const [borderWidth, setBorderWidth] = useState(0);
  return (
    <ScrollView style={{backgroundColor: theme.backgroundColor}}>
      <View
        style={{
          padding: 20,
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <BackButton
          onButtonClick={() => {navigation.goBack()}}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
        />
        <Text
          style={{fontFamily: 'Oxygen-Bold', fontSize: 18, color: '#050B31'}}>
          Where
        </Text>
        <BackButton
          onButtonClick={() => {}}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
          icon="close"
        />
      </View>

      <Text
        style={{
          flex: 1,
          paddingLeft: 25,
          justifyContent: 'flex-start',
          fontSize: 25,
          fontWeight: 'bold',
          paddingBottom: 10,
          color: theme.primaryTextColor,
        }}>
        Address
      </Text>
      <View style={{paddingLeft: 20, paddingRight: 20}}>
        <Searchbar
          style={{
            height: 60,
            borderWidth: borderWidth,
          }}
          placeholder="Post Code,Place Name"
          onChangeText={value => {
            setSearchAddress(value);
          }}
          onSubmitEditing={() => {
            if (searcAddress.length == 0) {
              setSearchComplete(false);
              setBorderWidth(0);
            } else {
              setSearchComplete(true);
              setBorderWidth(2);
            }
          }}
          autoComplete="name"
          value={searcAddress}></Searchbar>
      </View>
      {searchComplete && (
        <View>
          <Image
            style={{height: 300, margin: 20}}
            source={{
              uri: 'https://cdn.vox-cdn.com/thumbor/NSZrHXIBbcYFkXaoYwy91vUiYWA=/250x250/cdn.vox-cdn.com/uploads/chorus_asset/file/16256389/IMG_0003.jpg',
            }}
          />
          <CmButton
            label={'Countinue'}
            buttonStyle={{
              backgroundColor: theme.buttonColors.primary.active,
              marginHorizontal: 60,
              marginTop: 20,
            }}
            buttonFocusedColor={theme.buttonColors.primary.focused}
            buttonLabelStyle={{
              color: 'white',
            }}
            onClick={() => {
              navigation.navigate('MoreInfo')
            }}
          />
        </View>
      )}
    </ScrollView>
  );
}

export default CreateWhere;
