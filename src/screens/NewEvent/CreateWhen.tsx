import React, {useState} from 'react';
import {Dimensions, ScrollView, Text, View} from 'react-native';
import SelectDropdown from 'react-native-select-dropdown';
import BackButton from '../../components/BackButton';
import {useTheme} from '../../context/ThemeProvider';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Calender from '../../components/Calendar';
import {Button} from 'react-native-paper';
import CmButton from '../../components/CmButton';

function timeGenerate(startIndex: number) {
  var number = Array();
  for (let i = startIndex; i <= 24; i++) {
    if (i >= 0 && i < 10) {
      number.push('0' + i + ':00');
    } else {
      number.push(i + ':00');
    }
  }
  return number;
}

function CreateWhen({navigation}:any) {
  const [startTime, setStartTime] = useState();
  const [endTime, setEndTime] = useState();
  const [startTimeIndex, setStartTimeIndex] = useState(0);
  const [halfCalenderStatus, setHalfCalenderStatus] = useState(false);
  const [calendarValue, setCalendarValue] = useState();
  const theme = useTheme();

  return (
    <ScrollView style={{backgroundColor: theme.backgroundColor}}>
      <View
        style={{
          padding: 20,
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <BackButton
          onButtonClick={() => {navigation.goBack()}}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
        />
        <Text
          style={{fontFamily: 'Oxygen-Bold', fontSize: 18, color: '#050B31'}}>
          When
        </Text>
        <BackButton
          onButtonClick={() => {}}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
          icon="close"
        />
      </View>
      <Text
        style={{
          fontFamily: 'Oxygen-Bold',
          fontSize: 18,
          color: '#050B31',
          padding: 20,
        }}>
        Some description here..
      </Text>
      <Calender
        calenderData={(data: any) => {
          setHalfCalenderStatus(data.half);
          data.date != null ? setCalendarValue(data.date) : null;
        }}
      />
      {halfCalenderStatus && true && (
        <View
          style={{
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            padding: 10,
            marginHorizontal: 10,
          }}>
          <View>
            <Text
              style={{
                fontFamily: 'Oxygen-Bold',
                fontSize: 18,
                color: '#050B31',
                marginBottom: 10,
              }}>
              From
            </Text>
            <View
              style={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <SelectDropdown
                buttonStyle={{
                  width: Dimensions.get('window').width * 0.4,
                  backgroundColor: 'white',
                  borderWidth: 2,
                  height: 50,
                }}
                dropdownBackgroundColor="white"
                renderDropdownIcon={() => (
                  <Icon
                    name="chevron-down"
                    size={25}
                    style={{
                      fontFamily: 'Light',
                      color: theme.backButton.iconColor,
                    }}
                  />
                )}
                dropdownStyle={{borderWidth: 2}}
                defaultButtonText={'Time'}
                data={timeGenerate(0)}
                onSelect={(selectedItem, index) => {
                  setStartTimeIndex(index);
                  setStartTime(selectedItem);
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                  return selectedItem;
                }}
                rowTextForSelection={(item, index) => {
                  return item;
                }}
              />
              <Icon
                name="arrow-right"
                size={25}
                style={{
                  fontFamily: 'Light',
                  alignSelf: 'center',
                  margin: 10,
                }}
              />
            </View>
          </View>

          <View>
            <Text
              style={{
                fontFamily: 'Oxygen-Bold',
                fontSize: 18,
                color: '#050B31',
                marginBottom: 10,
              }}>
              To
            </Text>
            <SelectDropdown
              buttonStyle={{
                width: Dimensions.get('window').width * 0.4,
                backgroundColor: 'white',
                borderWidth: 2,
                height: 50,
              }}
              dropdownStyle={{borderWidth: 2}}
              dropdownBackgroundColor="white"
              renderDropdownIcon={() => (
                <Icon
                  name="chevron-down"
                  size={25}
                  style={{
                    fontFamily: 'Light',
                    color: theme.backButton.iconColor,
                  }}
                />
              )}
              defaultButtonText={'Time'}
              data={timeGenerate(startTimeIndex)}
              onSelect={(selectedItem, index) => {
                setEndTime(selectedItem);
              }}
              buttonTextAfterSelection={(selectedItem, index) => {
                return selectedItem;
              }}
              rowTextForSelection={(item, index) => {
                return item;
              }}
            />
          </View>
        </View>
      )}
      {!!startTime && !!endTime && halfCalenderStatus && (
        <CmButton
          label={'Countinue'}
          buttonStyle={{
            backgroundColor: theme.buttonColors.primary.active,
            marginHorizontal: 70,
            marginTop: 20,
          }}
          buttonFocusedColor={theme.buttonColors.primary.focused}
          buttonLabelStyle={{
            color: 'white',
          }}
          onClick={() => {
            navigation.navigate('CreateWhere')
          }}
        />
      )}
    </ScrollView>
  );
}

export default CreateWhen;
