import React, {useState} from 'react';
import {ScrollView, Text, TouchableOpacity, View} from 'react-native';
import {useTheme} from '../../context/ThemeProvider';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Appbar, Searchbar, TextInput, Button} from 'react-native-paper';
import {Chip, FAB} from 'react-native-elements';
import BackButton from '../../components/BackButton';
import CmButton from '../../components/CmButton';

function NewEvent({navigation}:any) {
  const theme = useTheme();
  const sports = [
    'Cricket',
    'Chess',
    'Hockey',
    'Cards',
    'VolleyBall',
    'Tennis',
    'Ludo',
  ];
  const farming = ['Planting', 'Plucking', 'Watering', 'Spraying', 'Seeding'];
  const [eventName, seteventName] = useState(String);
  const [sportsFilterd, setSportsFilterd] = useState(sports);
  const [farmingFilterd, setFarmingFilterd] = useState(farming);
  const [activityDisplay, setActivityDisplay] = useState(false);
  const [activityName, setActivityName] = useState(String);
  const [countinueStatus, setCountinueStatus] = useState(false);
  const [showAcivitis, setShowActivities] = useState(true);

  return (
    <ScrollView
      style={{backgroundColor: theme.backgroundColor, height: '100%'}}
      automaticallyAdjustContentInsets={true}>
      <View
        style={{
          padding: 20,
          paddingTop: 30,
          flexDirection: 'row',
          justifyContent: 'space-between',

          //alignItems: 'flex-start',
        }}>
        <Text
          style={{
            flex: 1,
            textAlign: 'center',
            justifyContent: 'flex-end',
            alignSelf: 'center',
            fontSize: 25,
            fontWeight: 'bold',
            color: theme.primaryTextColor,
          }}>
          Name
        </Text>
        <BackButton
          onButtonClick={() => {navigation.goBack()}}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
          icon="close"
        />
      </View>

      <Text
        style={{
          paddingLeft: 25,
          justifyContent: 'flex-start',
          fontSize: 25,
          fontWeight: 'bold',
          paddingBottom: 10,
          color: theme.primaryTextColor,
        }}>
        Name
      </Text>
      <View style={{paddingLeft: 20, paddingRight: 20}}>
        <TextInput
          style={{
            height: 60,
            borderColor: 'black',
            borderWidth: eventName.length > 0 ? 2 : 0,
            marginBottom: 20,
            backgroundColor: 'white',
          }}
          placeholder="Event Name"
          underlineColor="white"
          onChangeText={text => {
            seteventName(text);
          }}
          onEndEditing={() => {
            if (!!eventName) {
              setActivityDisplay(true);
            } else {
              setActivityDisplay(false);
              //  Alert.alert("Please enter the text to proceed");
            }
          }}
          value={eventName}
        />
      </View>
      {activityDisplay ? (
        <View style={{paddingLeft: 25, paddingRight: 25}}>
          <Text
            style={{
              justifyContent: 'flex-start',
              fontSize: 25,
              fontWeight: 'bold',
              color: theme.primaryTextColor,
              paddingBottom: 20,
            }}>
            Activity
          </Text>

          <Searchbar
            style={{
              height: 60,
              borderWidth: activityName.length > 0 ? 2 : 0,
            }}
            clearIcon={() =>
              !!activityName ? (
                <Icon
                  name="close-circle"
                  size={25}
                  style={{
                    fontFamily: 'Light',
                    color: 'black',
                  }}
                  onPress={() => {
                    setActivityName('');
                    setShowActivities(true);
                    setFarmingFilterd(farming);
                    setSportsFilterd(sports);
                  }}
                />
              ) : null
            }
            placeholder="Choose an Activity Category"
            onEndEditing={() => {
              if (!!activityName) {
                setCountinueStatus(true);
              } else {
                setCountinueStatus(false);
              }
            }}
            onChangeText={value => {
              const tempSports = sports.filter(item => {
                if (item.toLowerCase().match(value.toLowerCase())) {
                  return item;
                }
              });
              const tempFarming = farming.filter(item => {
                if (item.toLowerCase().match(value.toLowerCase())) {
                  return item;
                }
              });
              setSportsFilterd(tempSports);
              setFarmingFilterd(tempFarming);
              setActivityName(value);
            }}
            autoComplete="name"
            value={activityName}></Searchbar>

          {showAcivitis ? (
            <ScrollView style={{height: 250, marginTop: 10}}>
              {sportsFilterd.length > 0 ? (
                <Text
                  style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    fontSize: 18,
                    fontWeight: '400',
                    color: theme.primaryTextColor,
                    paddingBottom: 10,
                    paddingTop: 15,
                  }}>
                  Sports
                </Text>
              ) : null}
              <View
                style={{
                  borderRadius: 4,
                  borderColor: 'black',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                }}>
                {sportsFilterd.map((text, index) => (
                  <Chip
                    title={text}
                    key={index}
                    type="outline"
                    titleStyle={{
                      fontSize: 18,
                      color: 'black',
                      fontWeight: 'bold',
                    }}
                    onPress={() => {
                      setActivityName(text);
                      setShowActivities(false);
                    }}
                    buttonStyle={{borderWidth: 2, borderColor: 'black'}}
                    containerStyle={{
                      marginVertical: 5,
                      paddingLeft: 5,
                      paddingRight: 5,
                    }}
                  />
                ))}
              </View>

              {farmingFilterd.length > 0 ? (
                <Text
                  style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    fontSize: 18,
                    fontWeight: '400',
                    color: theme.primaryTextColor,
                    paddingBottom: 10,
                    paddingTop: 15,
                  }}>
                  Farming
                </Text>
              ) : null}
              <View
                style={{
                  borderRadius: 4,
                  borderColor: 'black',
                  flexDirection: 'row',
                  flexWrap: 'wrap',
                }}>
                {farmingFilterd.map((text, index) => (
                  <Chip
                    key={index}
                    title={text}
                    type="outline"
                    onPress={() => {
                      setActivityName(text);
                      setShowActivities(false);
                    }}
                    buttonStyle={{borderWidth: 2, borderColor: 'black'}}
                    titleStyle={{
                      fontSize: 18,
                      color: 'black',
                      fontWeight: 'bold',
                    }}
                    containerStyle={{
                      marginVertical: 5,
                      paddingLeft: 5,
                      paddingRight: 5,
                    }}
                  />
                ))}
              </View>

              {sportsFilterd.length == 0 && farmingFilterd.length == 0 && (
                <Text
                  style={{
                    flex: 1,
                    justifyContent: 'flex-start',
                    fontSize: 18,
                    color: theme.primaryTextColor,
                    paddingBottom: 10,
                    paddingTop: 15,
                  }}>
                  No search found
                </Text>
              )}
            </ScrollView>
          ) : null}
        </View>
      ) : null}
      {!showAcivitis ? (
      <CmButton
      label={'Countinue'}
      buttonStyle={{
        backgroundColor: theme.buttonColors.primary.active,
        marginHorizontal: 60,
        marginTop: 20,
      }}
      buttonFocusedColor={theme.buttonColors.primary.focused}
      buttonLabelStyle={{
        color: 'white',
      }}
      onClick={() => {
           navigation.navigate('CreateWhen')
      }}
    />
      ) : null}
    </ScrollView>
  );
}

export default NewEvent;
