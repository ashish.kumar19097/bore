import React, {useState} from 'react';
import {Dimensions, FlatList, ScrollView, TextInput, View} from 'react-native';
import {Divider, Input} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Button, Dialog, Text} from 'react-native-paper';
import SelectDropdown from 'react-native-select-dropdown';
import BackButton from '../../components/BackButton';
const levels = ['Basic', 'Medium', 'Easy', 'Advanced', 'Tough'];
import {useTheme} from '../../context/ThemeProvider';
import CmPopUp from '../../components/CmPopUp';
import CmButton from '../../components/CmButton';
function numbers(startIndex: number, endIndex: number) {
  var number = Array();
  for (let i = startIndex; i < endIndex; i++) {
    number.push(i);
  }
  return number;
}
function MoreInfo({navigation}:any) {
  const theme = useTheme();
  const [minPeople, setMinPeople] = React.useState();
  const [maxPeople, setMaxPeople] = React.useState();
  const [level, setLevel] = React.useState();
  const [fee, setFee] = React.useState(String);
  const [inputSize, setInputSize] = React.useState(0);
  const [popUpVisible, setPopUpVisible] = React.useState(false);
  return (
    <ScrollView
      style={{backgroundColor: theme.backgroundColor, height: '100%'}}>
      <View
        style={{
          padding: 20,
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <BackButton
          onButtonClick={() => {navigation.goBack()}}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
        />
        <Text
          style={{fontFamily: 'Oxygen-Bold', fontSize: 18, color: '#050B31'}}>
          More info
        </Text>
        <BackButton
          onButtonClick={() => {}}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
          icon="close"
        />
      </View>
      <Text
        style={{
          fontFamily: 'Oxygen-Bold',
          fontSize: 18,
          color: '#050B31',
          padding: 20,
        }}>
        Some description here..
      </Text>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 10,
          marginHorizontal: 10,
        }}>
        <View>
          <Text
            style={{
              fontFamily: 'Oxygen-Bold',
              fontSize: 18,
              color: '#050B31',
              marginBottom: 10,
            }}>
            Min.People
          </Text>
          <SelectDropdown
            buttonStyle={{
              width: Dimensions.get('window').width * 0.4,
              backgroundColor: 'white',
              borderWidth: 2,
              height: 50,
            }}
            dropdownBackgroundColor="white"
            renderDropdownIcon={() => (
              <Icon
                name="chevron-down"
                size={25}
                style={{fontFamily: 'Light', color: theme.backButton.iconColor}}
              />
            )}
            dropdownStyle={{borderWidth: 2}}
            defaultButtonText={'Select'}
            data={numbers(1, 30)}
            onSelect={(selectedItem, index) => {
              setMinPeople(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              return item;
            }}
          />
        </View>
        <View>
          <Text
            style={{
              fontFamily: 'Oxygen-Bold',
              fontSize: 18,
              color: '#050B31',
              marginBottom: 10,
            }}>
            Max.People
          </Text>
          <SelectDropdown
            buttonStyle={{
              width: Dimensions.get('window').width * 0.4,
              backgroundColor: 'white',
              borderWidth: 2,
              height: 50,
            }}
            dropdownStyle={{borderWidth: 2}}
            dropdownBackgroundColor="white"
            renderDropdownIcon={() => (
              <Icon
                name="chevron-down"
                size={25}
                style={{fontFamily: 'Light', color: theme.backButton.iconColor}}
              />
            )}
            defaultButtonText={'Select'}
            data={numbers(!!minPeople ? minPeople : 0, 50)}
            onSelect={(selectedItem, index) => {
              setMaxPeople(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              return item;
            }}
          />
        </View>
      </View>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 10,
          marginHorizontal: 10,
        }}>
        <View>
          <Text
            style={{
              fontFamily: 'Oxygen-Bold',
              fontSize: 18,
              color: '#050B31',
              marginBottom: 10,
            }}>
            Level
          </Text>
          <SelectDropdown
            dropdownStyle={{borderWidth: 2}}
            dropdownBackgroundColor="white"
            buttonStyle={{
              width: Dimensions.get('window').width * 0.4,
              backgroundColor: 'white',
              borderWidth: 2,
              height: 50,
            }}
            renderDropdownIcon={() => (
              <Icon
                name="chevron-down"
                size={25}
                style={{fontFamily: 'Light', color: theme.backButton.iconColor}}
              />
            )}
            defaultButtonText="Select"
            data={levels}
            onSelect={(selectedItem, index) => {
              setLevel(selectedItem);
            }}
            buttonTextAfterSelection={(selectedItem, index) => {
              return selectedItem;
            }}
            rowTextForSelection={(item, index) => {
              return item;
            }}
          />
        </View>
        <View>
          <Text
            style={{
              fontFamily: 'Oxygen-Bold',
              fontSize: 18,
              color: '#050B31',
              marginBottom: 10,
            }}>
            Fee
          </Text>
          <View>
            <TextInput
              placeholder="00"
              onChangeText={text => {
                setFee(text);
              }}
              style={{
                width: Dimensions.get('window').width * 0.4,
                backgroundColor: 'white',
                borderWidth: 2,
                height: 50,
                padding: 15,
              }}
              keyboardType="number-pad"
            />
          </View>
        </View>
      </View>
      <View
        style={{
          display: 'flex',
          flexDirection: 'row',
          justifyContent: 'space-between',
          padding: 10,
          marginHorizontal: 10,
        }}>
        <View>
          <Text
            style={{
              fontFamily: 'Oxygen-Bold',
              fontSize: 18,
              color: '#050B31',
              marginBottom: 10,
            }}>
            Details
          </Text>
        </View>
        <View>
          <Text
            style={{
              fontSize: 14,
              color: '#050B31',
              marginBottom: 10,
            }}>
            {inputSize}/1200
          </Text>
        </View>
      </View>
      <View
        style={{
          borderWidth: 2,
          marginRight: 20,
          marginLeft: 20,
          backgroundColor: 'white',
        }}>
        <TextInput
          style={{
            padding: 10,
            height: 90,
            justifyContent: 'flex-start',
            textAlignVertical: 'top',
            textAlign: 'auto',
            backgroundColor: 'white',
            fontSize: 20,
          }}
          placeholder="Type something"
          placeholderTextColor="grey"
          numberOfLines={10}
          multiline={true}
          maxLength={1200}
          onChangeText={text => {
            setInputSize(text.length);
          }}
        />
        <Icon
          name="animation-outline"
          size={25}
          style={{
            fontFamily: 'Light',
            alignSelf: 'flex-end',
            margin: 20,
          }}
        />
      </View>
      {!!maxPeople && !!minPeople && !!level && fee != '' && (
        <CmButton
          label={'Create Event'}
          buttonStyle={{
            backgroundColor: theme.buttonColors.primary.active,
            marginHorizontal: 60,
            marginTop: 20,
          }}
          buttonFocusedColor={theme.buttonColors.primary.focused}
          buttonLabelStyle={{
            color: 'white',
          }}
          onClick={() => {
            setPopUpVisible(true);
          }}
        />
      )}
      {popUpVisible && (
        <CmPopUp
          title="Create the event"
          body=" This is the kjhsadhaslkdja;ljsfenfalishcawlksncaoiswqnfdoiashcajwnfoiqhawfnasoihcasnoi"
          yesButtonText="Create the event"
          cancelButtonText="No"
          onYesButtonClick={() => {
            setPopUpVisible(false);
          }}
          onCancelButtonClick={() => {
            setPopUpVisible(false);
          }}
        />
      )}
    </ScrollView>
  );
}

export default MoreInfo;
