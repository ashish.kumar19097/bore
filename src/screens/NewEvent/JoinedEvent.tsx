import { View, Text, ScrollView, Image, Dimensions } from 'react-native'
import React from 'react'
import CmButton from '../../components/CmButton';
import BackButton from '../../components/BackButton';
import { useTheme } from '../../context/ThemeProvider';
import EventBriefDetails from '../../components/EvenBriefDetails';
import Location from '../../components/Location';
import IconCopy from 'react-native-vector-icons/Foundation';
import EventDescriptions from '../../components/EventDescriptions';
import CancelPolicies from '../../components/CancelPolicies';
import { Dialog } from 'react-native-paper';

const JoinedEvent = () => {
    const theme = useTheme();
    const windowWidth = Dimensions.get('window').width;

    const [visible, setVisible] = React.useState(false);

    const showDialog = () => setVisible(true);
    const hideDialog = () => setVisible(false);

    return (
        <View style={{flex: 1, backgroundColor: "white"}}>
            <View style={{padding: 20, flexDirection: "row", alignItems: "center"}}>
                <BackButton buttonColor={theme.backgroundColor} iconColor={theme.backButton.iconColor} onButtonClick={()=>{}}/>
                <Text style={{fontFamily: "Oxygen-Bold", color: "black",fontSize: 18,textAlign: "center", flex: 1, marginRight: "5%"}}>EventTitle...</Text>
            </View>
            <ScrollView>
                <View style={{height: 216, borderColor: "red", width: windowWidth, justifyContent: "center", marginTop: 10}}>
                    <Image
                        source={require('../../../assets/images/Large.png')}
                        style={{
                            resizeMode: "contain",
                            width: "100%",
                            height: undefined, 
                            aspectRatio: 1
                        }}
                    />
                </View>
                <View style={{marginTop: 20, marginHorizontal: 30}}>
                    <EventBriefDetails />
                </View>
                <View style={{marginHorizontal: 20}}>
                    <Location />
                </View>
                <View style={{flexDirection: "row", justifyContent: "center", padding: 20}}>
                    <IconCopy name="page-copy" size={20} color={"#894CED"} style={{fontFamily:"Light"}}/>
                    <Text style={{fontFamily: "Oxygen-Bold", fontSize: 14, color: "#894CED", marginLeft: 10}}>Copy the address</Text>
                </View>
                <CmButton label={'Show route'} buttonStyle={{borderWidth: 2, borderColor: '#894CED', marginHorizontal: 60}} buttonFocusedColor={''} buttonLabelStyle={{color: "#894CED", fontFamily: "Oxygen-Bold"}} onClick={()=>{}}/>
                <View style={{padding: 20}}>
                    <EventDescriptions />
                    <View style={{marginTop: 15}}></View>
                    <CancelPolicies text={''} />
                    <View style={{marginTop: 15, borderWidth: 1, borderColor:"#F2F4FE"}}></View>
                </View>
                <View style={{marginHorizontal: 20}}>
                    <Text style={{color: "#000000", fontFamily: "Oxygen-Regular", fontWeight: "bold", fontSize: 16, marginLeft: 3}}>Ashish</Text>
                    <View style={{flexDirection: "row", marginTop: 5}}>
                        <Image source={require('../../../assets/images/Artwork.png')}></Image>
                        <Text style={{color: "#000000", fontFamily: "Oxygen-Regular", fontWeight: "bold", fontSize: 16 , marginLeft: 7}}>Qualified owner</Text>
                    </View>
                </View>
                <CmButton label={'Contact Owner'} buttonStyle={{marginTop: 20,borderWidth: 2, borderColor: '#894CED', marginHorizontal: 60}} buttonFocusedColor={''} buttonLabelStyle={{color: "#894CED", fontFamily: "Oxygen-Bold"}} onClick={()=>{}}/>
                <View style={{padding: 25}}>
                    <Text style={{fontFamily: "Oxygen-Regular", fontSize: 14}}>Need more help?</Text>
                    <Text style={{fontFamily: "Oxygen-Regular", fontSize: 18, color: "black", marginTop: 20}}>Contact us</Text>
                </View>
            </ScrollView>
            
            <View style={{height: 100, justifyContent: "center"}}>
                <CmButton label={'Cancel the event'} buttonStyle={{borderWidth: 2, borderColor: '#894CED', marginHorizontal: 25}} buttonFocusedColor={''} buttonLabelStyle={{color: "#894CED", fontFamily: "Oxygen-Bold"}} onClick={()=>showDialog()}/>
            </View>
            <Dialog style={{bottom: 20}} visible={visible} onDismiss={hideDialog}>
                <Dialog.Title
                    style={{
                        fontSize: 28,
                        fontFamily: 'Oxygen-Bold',
                        fontWeight: 'bold',
                        color: '#000000',
                    }}>
                    Join the event
                </Dialog.Title>
                <Dialog.Content>
                    <Text
                        style={{
                        fontSize: 16,
                        fontFamily: 'Oxygen-Regular',
                        color: '#000000',
                        }}>
                        Proin ut dui sed metus pharetra hend rerit vel non mi. Nulla ornare
                        faucibus ex, non facilisis nisl. Maecenas aliquet mauris ut tempus
                    </Text>
                </Dialog.Content>
        <Dialog.Actions style={{width: '100%', padding: 10}}>
          <CmButton
            label={'I want to cancel'}
            buttonStyle={{
              backgroundColor: theme.buttonColors.primary.active,
              flex: 1,
            }}
            buttonFocusedColor={''}
            buttonLabelStyle={{
              color: 'white',
              fontFamily: 'Oxygen-Bold',
              fontSize: 14,
            }}
            onClick={function () {
              throw new Error('Function not implemented.');
            }}
          />
          <View style={{marginHorizontal: 10}}></View>
          <CmButton
            label={'No'}
            buttonStyle={{
              borderWidth: 1,
              borderColor: theme.buttonColors.outline.active,
              flex: 1,
            }}
            buttonFocusedColor={''}
            buttonLabelStyle={{fontFamily: 'Oxygen-Bold', fontSize: 14}}
            onClick={function () {
              throw new Error('Function not implemented.');
            }}
          />
        </Dialog.Actions>
      </Dialog>
        </View>
    )
}

export default JoinedEvent;