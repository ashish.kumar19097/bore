import React from 'react'
import { StyleProp, Text, useWindowDimensions, View } from 'react-native';
import { color } from 'react-native-elements/dist/helpers';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { useTheme } from '../context/ThemeProvider';
import UseOrientation from '../utils/UseOrientaion';



const Dummpy = () => {
    const orient = UseOrientation();
    const { height, width } = useWindowDimensions();
    const theme = useTheme();
    // console.log(width)
    // console.log(height)
    return (
        <View style={{marginTop: `${(71 * 100)/height}%`, backgroundColor: theme.backgroundColor}}>
            <Text style={{color: "red"}}>{orient}</Text>
            <Text>{width}</Text>
            <View style={{height: 400, backgroundColor: "red"}}></View>
        </View>
    );
}

export default Dummpy;