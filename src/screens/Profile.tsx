import React from 'react';
import {View, Text, ScrollView} from 'react-native';
import {Divider} from 'react-native-elements';
import {Avatar, Card, IconButton} from 'react-native-paper';
import Calender from '../components/Calendar';
import {useTheme} from '../context/ThemeProvider';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CmButton from '../components/CmButton';

const Profile = ({navigation, route}: any) => {
  const theme = useTheme();
  return (
    <ScrollView style={{backgroundColor: 'white'}}>
      <View>
        <Text
          style={{
            flex: 1,
            textAlign: 'left',
            justifyContent: 'flex-end',
            marginLeft: 20,
            marginTop: 20,
            fontSize: 22,
            fontFamily: 'Oxygen',
            fontWeight: '600',
            color: theme.primaryTextColor,
          }}>
          Chuan
        </Text>
        <Text
          style={{
            flex: 1,
            textAlign: 'left',
            justifyContent: 'flex-end',
            marginLeft: 20,
            marginTop: 10,
            marginBottom: 10,
            fontSize: 20,
            fontFamily: 'Oxygen',
            fontWeight: 'bold',
            color: !!route.params ? 'black' : theme.backButton.iconColor,
          }}>
          {!!route.params
            ? !!route.params.verified
              ? 'Verified'
              : 'Qualifed owner'
            : 'Not verify yet'}
        </Text>
        <Divider
          orientation="vertical"
          width={100}
          style={{marginLeft: 30, marginRight: 30}}
        />
        {!!route.params ? (
          route.params.host ? (
            <View style={{marginHorizontal: 20}}>
              <Text>My host event</Text>
              <Text style={{margin: 20, color: 'black', fontSize: 17}}>
                You do not have your own event yet
              </Text>
              <CmButton
                label={'Host my Event'}
                buttonStyle={{
                  borderColor: theme.buttonColors.primary.active,
                  borderWidth: 2.5,
                  marginHorizontal: 40,
                }}
                buttonFocusedColor={''}
                buttonLabelStyle={{
                  fontFamily: 'Oxygen-Regular',
                  fontWeight: 'bold',
                  fontSize: 14,
                  color: theme.ActionTextColor,
                }}
                onClick={() => {
                  navigation.navigate('NewEvent');
                }}
              />
            </View>
          ) : (
            <Card
              onPress={() => {
                !!route.params
                  ? navigation.navigate('AddCard', {owner: 'true'})
                  : navigation.navigate('AddCard');
              }}>
              <Card.Title
                style={{marginTop: 10, marginBottom: 10}}
                title={
                  !!route.params
                    ? 'Want to be an event owner?'
                    : 'Want to attend the event'
                }
                titleStyle={{
                  margin: 20,
                  fontSize: 18,
                  fontFamily: 'Oxygen',
                  fontWeight: 'normal',
                }}
                subtitleStyle={{
                  margin: 20,
                  fontFamily: 'Oxygen',
                  fontSize: 18,
                  fontWeight: 'bold',
                  color: theme.backButton.iconColor,
                }}
                subtitle={!!route.params ? 'Read more' : 'Add payment card'}
                left={() => (
                  <Avatar.Image
                    style={{backgroundColor: theme.backgroundColor}}
                  />
                )}
              />
            </Card>
          )
        ) : (
          <Card
            onPress={() => {
              !!route.params
                ? navigation.navigate('AddCard', {owner: 'true'})
                : navigation.navigate('AddCard');
            }}>
            <Card.Title
              style={{marginTop: 10, marginBottom: 10}}
              title={
                !!route.params
                  ? 'Want to be an event owner?'
                  : 'Want to attend the event'
              }
              titleStyle={{
                margin: 20,
                fontSize: 18,
                fontFamily: 'Oxygen',
                fontWeight: 'normal',
              }}
              subtitleStyle={{
                margin: 20,
                fontFamily: 'Oxygen',
                fontSize: 18,
                fontWeight: 'bold',
                color: theme.backButton.iconColor,
              }}
              subtitle={!!route.params ? 'Read more' : 'Add payment card'}
              left={() => (
                <Avatar.Image
                  style={{backgroundColor: theme.backgroundColor}}
                />
              )}
            />
          </Card>
        )}
        <Divider
          orientation="vertical"
          width={100}
          style={{marginLeft: 30, marginRight: 30}}
        />
        <Text
          style={{
            flex: 1,
            textAlign: 'left',
            justifyContent: 'flex-end',
            marginLeft: 20,
            marginTop: 20,
            fontSize: 14,
            fontFamily: 'Oxygen',
          }}>
          Calendar
        </Text>
        <View
          style={{
            borderRadius: 10,
            margin: 10,
            overflow: 'hidden',
          }}>
          <Calender
            fullCalenar={false}
            calenderData={(data: any) => {}}
            headerStyle={{fontWeight: 'bold', fontSize: 23}}
          />
          {!!route.params ? (
            <View
              style={{
                backgroundColor: 'black',
                padding: 15,
                flexDirection: 'row',
                justifyContent: 'space-between',
              }}>
              <View>
                <Text style={{color: 'white'}}>12:00-2:00 11/8(Mon)</Text>
                <Text style={{color: 'white', fontWeight: '700', fontSize: 18}}>
                  Event tile Event title Event title
                </Text>
              </View>

              <Icon
                name="arrow-right"
                size={25}
                style={{color: 'white', alignSelf: 'center'}}
                onPress={() => {}}
              />
            </View>
          ) : null}
        </View>

        <Text
          onPress={() => {
            navigation.navigate('PaymentMethod');
          }}
          style={{
            flex: 1,
            textAlign: 'left',
            justifyContent: 'flex-end',
            margin: 20,
            fontSize: 22,
            fontFamily: 'Oxygen-bold',
            fontWeight: '600',
            marginLeft: 35,
            color: theme.primaryTextColor,
          }}>
          Payment method
        </Text>
        <Divider
          orientation="vertical"
          width={100}
          style={{marginLeft: 30, marginRight: 30}}
        />
        <Text
          onPress={() => {}}
          style={{
            flex: 1,
            textAlign: 'left',
            justifyContent: 'flex-end',
            margin: 20,
            fontSize: 22,
            fontFamily: 'Oxygen',
            fontWeight: '600',
            marginLeft: 35,
            color: theme.primaryTextColor,
          }}>
          Help
        </Text>
        <Divider
          orientation="vertical"
          width={100}
          style={{marginLeft: 30, marginRight: 30}}
        />
        <Text
          style={{
            flex: 1,
            textAlign: 'left',
            justifyContent: 'flex-end',
            margin: 20,
            marginLeft: 35,
            fontSize: 22,
            fontFamily: 'Oxygen',
            fontWeight: '600',
            color: theme.primaryTextColor,
          }}>
          Policies
        </Text>
        <Divider
          orientation="vertical"
          width={100}
          style={{marginLeft: 30, marginRight: 30}}
        />
        <Text
          style={{
            flex: 1,
            textAlign: 'left',
            justifyContent: 'flex-end',
            margin: 20,
            marginLeft: 35,
            fontSize: 22,
            fontFamily: 'Oxygen',
            fontWeight: '600',
            color: theme.primaryTextColor,
          }}>
          Logout
        </Text>
      </View>
    </ScrollView>
  );
};

export default Profile;
