import {View, Text, ScrollView, StyleSheet} from 'react-native';
import React from 'react';
import AppLogo from '../components/AppLogo';
import {Description} from '../components/Description';
import imageLogoPath from '../constants/LogoImagePath';
import {useTheme} from '../context/ThemeProvider';
import CmTextInput from '../components/CmTextInput';
import CmButton from '../components/CmButton';

const Login = ({navigation}: any) => {
  const theme = useTheme();
  return (
    <ScrollView
      contentContainerStyle={{
        flexGrow: 1,
        flexDirection: 'column',
        justifyContent: 'space-between',
      }}
      style={{backgroundColor: theme.backgroundColor}}>
      <View style={styles.logoContainer}>
        <AppLogo
          bigHalfLetter={imageLogoPath.PurpleColor.bigHalfLetter}
          smallHalfLetter={imageLogoPath.PurpleColor.smallHalfLetter}></AppLogo>
      </View>
      <View style={styles.descriptionTextContainer}>
        <Description
          text={
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla interdum enim sed nisi faucibus feugiat.'
          }
        />
      </View>
      <View style={{marginTop: 55, marginHorizontal: 30}}>
        <CmTextInput
          placeHolder={'Email'}
          placeHolderTextColor={theme.placeHolderTextColor}
        />
        <CmTextInput
          placeHolder={'Password'}
          placeHolderTextColor={theme.placeHolderTextColor}
        />
      </View>
      <CmButton
        label={'Continue'}
        buttonStyle={{
          backgroundColor: theme.buttonColors.primary.active,
          marginHorizontal: 60,
          marginTop: 10,
        }}
        buttonFocusedColor={theme.buttonColors.primary.focused}
        buttonLabelStyle={{
          color: 'white',
        }}
        onClick={() => {
          navigation.navigate('Welcome');
        }}
      />
      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-between',
          marginTop: 80,
          bottom: 20,
          marginHorizontal: 30,
          flex:1,

        }}>
        <Text
          style={{
            color: theme.ActionTextColor,
            fontFamily: 'Oxygen-Regular',
            fontSize: 18,
      
          }}
          onPress={() => {}}>
          Forgot Password?
        </Text>
        <Text
          style={{
            color: theme.ActionTextColor,
            fontFamily: 'Oxygen-Regular',
            fontSize: 18,

          }}
          onPress={() => {navigation.goBack()}}>
          Register here
        </Text>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  logoContainer: {
    marginLeft: 41,
    marginTop: 62,
  },

  descriptionTextContainer: {
    marginTop: 20,
    marginHorizontal: 41,
  },
});

export default Login;
