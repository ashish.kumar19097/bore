import React from "react";
import { ScrollView, StyleSheet, useWindowDimensions, View } from "react-native";
import AppLogo from "../components/AppLogo";
import CmButton from "../components/CmButton";
import { Description } from "../components/Description";
import imageLogoPath from '../constants/LogoImagePath'
import { useTheme } from "../context/ThemeProvider";
import UseOrientation from "../utils/UseOrientaion";

const RegisterOrLogin = ({ navigation }:any) => {
    const theme = useTheme();
    const orienatation = UseOrientation()
    const dim = orienatation ? useWindowDimensions().height : useWindowDimensions().width; 

    return (
        <ScrollView contentContainerStyle={{flexGrow:1, justifyContent: 'space-between', flexDirection: 'column'}} style={{backgroundColor:theme.backgroundColor}}>
            <View style={styles.logoContainer}>
                <AppLogo bigHalfLetter={imageLogoPath.PurpleColor.bigHalfLetter} smallHalfLetter={imageLogoPath.PurpleColor.smallHalfLetter}  ></AppLogo>
                <Description Textstyle={{marginTop: 10}} text={"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla interdum enim sed nisi faucibus feugiat."} />
            </View>
            <View style={{marginHorizontal: 60, flex: 1, justifyContent: "flex-end"}}>
                <CmButton 
                    label={"Register"}
                    buttonStyle={{
                        backgroundColor: theme.buttonColors.primary.active,
                    }}
                    buttonFocusedColor={theme.buttonColors.primary.focused} 
                    buttonLabelStyle={{
                        color: "white"
                    }} 
                    onClick={()=>{navigation.navigate('Register')}}
                />
            </View>
            <CmButton 
                label={"Login"}
                buttonStyle={{
                    marginTop: 15,
                    marginHorizontal: 60,
                    backgroundColor: "white",
                    borderColor: theme.buttonColors.primary.active,
                    borderWidth: 1,
                    marginBottom: 60
                }}
                onClick={()=>{navigation.navigate('Login')}} 
                buttonFocusedColor={theme.buttonColors.outline.focused} 
                buttonLabelStyle={{
                    color: theme.buttonColors.primary.active
                }}            
            />
        </ScrollView>
    );
}

const styles = StyleSheet.create({
    logoContainer : {
        marginHorizontal: 41,
        marginTop: 62
    },
});

export default RegisterOrLogin;