import React from 'react';
import {ScrollView, StatusBar, Text, View} from 'react-native';
import {useTheme} from '../context/ThemeProvider';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Profile from './Profile';
import Search from './Search';
import Chat from './Chat';
import {Home} from './Home';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import {Button} from 'react-native-paper';

const Tab = createBottomTabNavigator();

export const DashBoard = () => {
  const theme = useTheme();
  const statusHeight = StatusBar.currentHeight;

  return (
    <Tab.Navigator
      screenOptions={{
        tabBarShowLabel: false,
        headerShown: false,
        tabBarIconStyle: {
          flex: 1,
          justifyContent: "center",
          alignItems: "center"
        },
        tabBarStyle: {
          height: 80,
        },
      }}>
      <Tab.Screen
        name="Home"
        component={Home}
        options={{
          tabBarLabel: 'Home',
          tabBarIcon: ({focused}) => {
            return (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="apps"
                  size={35}
                  color={
                    focused
                      ? theme.bottomNavigation.activeIconColor
                      : theme.bottomNavigation.inactiveIconColor
                  }
                />
                <Text
                  style={{
                    color: focused
                      ? theme.bottomNavigation.activeIconColor
                      : theme.bottomNavigation.inactiveIconColor,
                  }}>
                  HOME
                </Text>
              </View>
            );
          },
        }}
      />
      <Tab.Screen
        name="Search"
        component={Search}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({focused}) => {
            return (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="magnify"
                  size={35}
                  color={
                    focused
                      ? theme.bottomNavigation.activeIconColor
                      : theme.bottomNavigation.inactiveIconColor
                  }
                />
                <Text
                  style={{
                    color: focused
                      ? theme.bottomNavigation.activeIconColor
                      : theme.bottomNavigation.inactiveIconColor,
                    fontFamily: 'Oxygen-Regular',
                    fontSize: 14,
                  }}>
                  SEARCH
                </Text>
              </View>
            );
          },
        }}
      />
      <Tab.Screen
        name="Chat"
        component={Chat}
        options={{
          tabBarLabel: 'Search',
          tabBarIcon: ({focused}) => {
            return (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="chat-processing-outline"
                  size={35}
                  color={
                    focused
                      ? theme.bottomNavigation.activeIconColor
                      : theme.bottomNavigation.inactiveIconColor
                  }
                />
                <Text
                  style={{
                    color: focused
                      ? theme.bottomNavigation.activeIconColor
                      : theme.bottomNavigation.inactiveIconColor,
                  }}>
                  CHAT
                </Text>
              </View>
            );
          },
        }}
      />
      <Tab.Screen
        name="Profile"
        component={Profile}
        options={{
          tabBarLabel: 'profile',
          tabBarIcon: ({focused}) => {
            return (
              <View
                style={{
                  flex: 1,
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
                <Icon
                  name="account-outline"
                  size={35}
                  color={
                    focused
                      ? theme.bottomNavigation.activeIconColor
                      : theme.bottomNavigation.inactiveIconColor
                  }
                />
                <Text
                  style={{
                    color: focused
                      ? theme.bottomNavigation.activeIconColor
                      : theme.bottomNavigation.inactiveIconColor,
                  }}>
                  PROFILE
                </Text>
              </View>
            );
          },
        }}
      />
    </Tab.Navigator>
  );
};
