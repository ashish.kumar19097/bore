import React from 'react';
import {
  Button,
  Dimensions,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableHighlight,
  View,
} from 'react-native';
import CmButton from '../components/CmButton';
import CmTextInput from '../components/CmTextInput';
import {Description} from '../components/Description';
import {useTheme} from '../context/ThemeProvider';

const Register: React.FC<{}> = ({navigation}: any) => {
  const theme = useTheme();
  return (
    <ScrollView
      automaticallyAdjustContentInsets={true}
      contentContainerStyle={{
        flexGrow: 1,
        backgroundColor: theme.backgroundColor,
      }}>
      <View style={{marginTop: 100, marginHorizontal: 30}}>
        <Description
          text={'Please enter your email address and Login password.'}
        />
      </View>
      <View style={{marginTop: 140, marginHorizontal: 30}}>
        <CmTextInput
          placeHolder={'Email'}
          placeHolderTextColor={theme.placeHolderTextColor}
        />
        <CmTextInput
          placeHolder={'Password'}
          placeHolderTextColor={theme.placeHolderTextColor}
        />
      </View>
      <CmButton
        label={'Continue'}
        buttonStyle={{
          backgroundColor: theme.buttonColors.primary.active,
          marginHorizontal: 60,
          marginTop: 10,
        }}
        buttonFocusedColor={theme.buttonColors.primary.focused}
        buttonLabelStyle={{
          color: 'white',
        }}
        onClick={() => {
          navigation.navigate('ConfirmRegister');
        }}
      />
      <View style={{marginHorizontal: 28, marginTop: 50, flexDirection: 'row'}}>
        <Text
          style={{
            fontFamily: 'Oxygen-Regular',
            fontSize: 18,
            color: '#050B31',
          }}>
          join us before?
        </Text>
        <Text
          onPress={() => {navigation.goBack()}}
          style={{
            fontFamily: 'Oxygen-Regular',
            fontSize: 18,
            marginHorizontal: 5,
            color: theme.ActionTextColor,
          }}>
          Log in
        </Text>
      </View>
    </ScrollView>
  );
};

export default Register;
