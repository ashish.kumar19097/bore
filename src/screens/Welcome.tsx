import React from "react";
import { ScrollView, StyleSheet, Text, TouchableHighlight, View } from "react-native";
import CmButton from "../components/CmButton";
import { Description } from "../components/Description";
import { useTheme } from "../context/ThemeProvider";

const Welcome = ({ navigation }:any) : JSX.Element => {
    const theme = useTheme();
    return (
        <ScrollView contentContainerStyle={{flexGrow: 1, backgroundColor: theme.backgroundColor, flexDirection: "column", justifyContent: "space-between"}}>
            <View style={{marginTop: 102}}>
                <Text 
                    style={{
                        fontFamily: "Oxygen-Bold", 
                        fontSize: 32,
                        textAlign: "center",
                        color: theme.welcomeTextColor
                    }}> Welcome!
                </Text>
            </View>
            <View style={{flex: 1, marginTop: 60,  marginHorizontal: 35, }}>
                <Description text={"You are almost there!"} />
                <Description text="Tell us more about youself so we can find the best activities for you."/>
            </View>
            <View style={{ flex: 1, marginTop: 20, marginHorizontal: 60}}>
                <CmButton 
                    label={"Let's go"} 
                    buttonStyle={{
                        backgroundColor : theme.buttonColors.primary.active,
                    }} 
                    buttonFocusedColor={theme.buttonColors.primary.focused} 
                    buttonLabelStyle={{
                        color: "white"
                    }} 
                    onClick={() => {navigation.navigate('Who')}}            
                />
            </View>
        </ScrollView>
    );
}

export default Welcome;
