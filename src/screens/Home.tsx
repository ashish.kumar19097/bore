import React, {useState} from 'react';
import {
  View,
  Text,
  FlatList,
  Image,
  TouchableOpacity,
  BackHandler,
  Animated,
} from 'react-native';
import {FAB} from 'react-native-paper';
import ActivityData from '../constants/eventDetails';
import EventDetails from '../components/EventDetails';
import HalfCalendar from './HalfCalendar';
import Calender from '../components/Calendar';

var offset = 0;
export const Home = () => {
  const [appBarHeight, setappBarHeight] = React.useState(220);
  const [date, setDate] = useState(new Date());
  const [appBarHidden, setAppBarHidden] = React.useState(true);
  return (
    <View style={{flex: 1,
      display: 'flex',
      justifyContent: 'space-around',}}>
      <View>
        <View
          style={{
            height: appBarHeight,
            transform: [{scaleX: 1.6}],
            borderBottomStartRadius: 200,
            borderBottomEndRadius: 200,
            backgroundColor: 'black',
          }}
        />

        {appBarHidden ? (
          <View
            style={{
              position: 'absolute',
              top: 0,
              backgroundColor: 'red',
              height: 100,
            }}>
            <HalfCalendar calenderData={() => {}} />
            <Text
              style={{
                color: 'yellow',
                textAlign: 'center',
                right: 50,
              }}>
              No event today.
            </Text>
            <Text
              style={{
                backgroundColor: 'transparent',
                color: 'yellow',
                textAlign: 'center',
                justifyContent: 'center',
                marginHorizontal: 50,
              }}>
              Up comming event on Monday
            </Text>
          </View>
        ) : (
          <Text
            style={{
              color: 'white',
              fontWeight: 'bold',
              fontSize: 23,
              textAlign: 'center',
              top: 10,
              position: 'absolute',
              alignSelf: 'center',
            }}>
            November,18 2022
          </Text>
        )}
      </View>
      {!appBarHidden ? (
        <FAB
          onPress={() => {
            setappBarHeight(200);
            setAppBarHidden(true);
          }}
          style={{
            position: 'absolute',
            justifyContent: 'center',
            alignItems: 'center',
            alignSelf: 'center',
            zIndex: 3,
            flex: 1,
            top: 45,
            backgroundColor: '#ffffff',
            height: 50,
            width: 50,
          }}
          small
          icon="arrow-down"></FAB>
      ) : null}
      <Text
        style={{
          margin: 20,
          fontSize: 20,
          fontWeight: '600',
          color: 'black',
        }}>
        Events for you
      </Text>
      {
        <FlatList
          onScrollEndDrag={() => {}}
          onScroll={event => {
            var currentOffset = event.nativeEvent.contentOffset.y;
            var direction = currentOffset > offset ? true : false;
            offset = currentOffset;
            direction ? (setappBarHeight(70), setAppBarHidden(false)) : null;
          }}
          data={ActivityData}
          renderItem={({item}) => {
            return <EventDetails eventDetails={item} />;
          }}
        />
      }
    </View>
  );
};
