import * as React from 'react';
import {FlatList, Image, View} from 'react-native';
import {LogBox} from 'react-native';
import {
  Button,
  Menu,
  Divider,
  Provider,
  Appbar,
  Text,
} from 'react-native-paper';
import BackButton from '../components/BackButton';
import EventDetails from '../components/EventDetails';

import ActivityData from '../constants/eventDetails';
import {useTheme} from '../context/ThemeProvider';

const Activities = ({navigation}: any) => {
  const theme = useTheme();
  const [visible, setVisible] = React.useState(false);
  const [activityData, setActivityData] = React.useState(ActivityData);
  const [sortPar, setSortPar] = React.useState('Date');

  const openMenu = () => setVisible(true);

  const closeMenu = () => setVisible(false);

  return (
    <>
      <View
        style={{
          flexDirection: 'row',
          alignItems: 'flex-end',
          width: '100%',
          backgroundColor: theme.backgroundColor,
          height: 50,
        }}>
        <BackButton
          onButtonClick={() => {
            LogBox.ignoreLogs(['EventEmitter.removeListener']);
            navigation.goBack();
          }}
          buttonColor={theme.backgroundColor}
          iconColor={theme.backButton.iconColor}
        />

        <Text
          style={{
            flex: 1,
            textAlign: 'center',
            justifyContent: 'flex-end',
            alignSelf: 'center',
            fontSize: 25,
            fontWeight: 'bold',
            paddingRight: 60,
          }}>
          Activities
        </Text>
      </View>
      <Provider>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'flex-end',
            backgroundColor: theme.backgroundColor,
            justifyContent: 'flex-end',
          }}>
          <Text
            style={{
              flex: 1,
              textAlign: 'left',
              justifyContent: 'flex-end',
              alignSelf: 'center',
              fontSize: 30,
              fontWeight: 'bold',
              paddingLeft: 20,
            }}>
            BasketBall
          </Text>
          <Menu
            visible={visible}
            onDismiss={closeMenu}
            statusBarHeight={40}
            contentStyle={{}}
            anchor={
              <Button uppercase={false} onPress={openMenu}>
                Sort By {sortPar}
              </Button>
            }>
            <Menu.Item
              style={sortPar == 'Date' ? {backgroundColor: '#A564F7'} : null}
              onPress={() => {
                setActivityData(
                  activityData.sort(
                    (n1, n2) => parseInt(n1.data) - parseInt(n2.data),
                  ),
                );
                setVisible(false);
                setSortPar('Date');
              }}
              title="By Date"
            />
            <Menu.Item
              style={
                sortPar == 'Distance' ? {backgroundColor: '#A564F7'} : null
              }
              onPress={() => {
                setActivityData(
                  activityData.sort(
                    (n1, n2) => parseInt(n1.distance) - parseInt(n2.distance),
                  ),
                );
                setVisible(false);
                setSortPar('Distance');
              }}
              title="By Distance"
            />
            <Menu.Item
              style={sortPar == 'Level' ? {backgroundColor: '#A564F7'} : null}
              onPress={() => {
                setActivityData(
                  activityData.sort(
                    (n1, n2) => parseInt(n1.level) - parseInt(n2.level),
                  ),
                );
                setVisible(false);
                setSortPar('Level');
              }}
              title="By Level"
            />
            <Menu.Item
              style={sortPar == 'Fee' ? {backgroundColor: '#A564F7'} : null}
              onPress={() => {
                setActivityData(
                  activityData.sort(
                    (n1, n2) => parseInt(n1.charges) - parseInt(n2.charges),
                  ),
                );
                setVisible(false);
                setSortPar('Fee');
              }}
              title="By Fee"
            />
          </Menu>
        </View>

        <FlatList
          style={{zIndex: 1, backgroundColor: theme.backgroundColor}}
          data={activityData}
          renderItem={({item}) => {
            return <EventDetails eventDetails={item} />;
          }}
        />
      </Provider>
    </>
  );
};

export default Activities;
