import React from 'react'
import { ScrollView, StyleSheet, Text, TouchableHighlight, View } from 'react-native';
import CmButton from '../components/CmButton';
import { Description } from '../components/Description';
import { useTheme } from '../context/ThemeProvider';

const ConfirmRegister = ({ navigation }:any) : JSX.Element => {
    const theme = useTheme();
    setTimeout(()=>{
      navigation.navigate('Welcome')
    },1000)
    return (
        <ScrollView contentContainerStyle={{flexGrow: 1, backgroundColor: theme.backgroundColor}}>
            <View style={{marginTop: 120, marginHorizontal: 30}}>
                <Description text={'An email is sent out.'} Textstyle={{fontFamily: "Oxygen-Regular"}} />
                <Description text={'Please confirm your email address.'} Textstyle={{fontFamily: "Oxygen-Regular"}} />
            </View>
            <View style={{marginTop: 278}}>
                <CmButton 
                    label={"Didn't receive the email"} 
                    buttonStyle={{
                        borderColor: theme.buttonColors.outline.active,
                        borderWidth: 2,
                        marginHorizontal: 60
                    }} 
                    buttonFocusedColor={theme.buttonColors.outline.focused}
                    buttonLabelStyle={{
                        fontFamily: "Oxygen-Bold",
                        fontSize: 16,
                        color: theme.ActionTextColor
                    }} 
                    onClick={() => {}}                
                />
            </View>
        </ScrollView>
    );
}

export default ConfirmRegister;

const styles = StyleSheet.create({

    textContainer : {
        position: "absolute",
        top: 120 - 62,
        marginHorizontal: 30,
        height: 116
    },

    textStyle : {
        fontSize: 21,
        lineHeight: 28,
        color: "white"
    },

    buttonContainer : {
        marginTop: 485 - 62,
        margin: 60,
        backgroundColor: "#F2F4FE",
        justifyContent: "center"
    },

    buttonTitleStyle: {
        color: "black",
        fontSize: 18,
        padding: 12,
        textAlign: "center",
    },
})