import React from 'react';
import { ScrollView, View } from 'react-native';
import BackButton from '../components/BackButton';
import CmButton from '../components/CmButton';
import { Description } from '../components/Description';
import { useTheme } from '../context/ThemeProvider';

export const Done = ({navigation}:any) => {
    const theme = useTheme()
    return (
        <ScrollView contentContainerStyle={{flexGrow: 1}} style={{backgroundColor: theme.backgroundColor}}>
            <BackButton 
                buttonStyle={{
                    marginTop: 57,
                    marginHorizontal: 20
                }}
                buttonColor={theme.backButton.backgroundColor}
                iconColor={theme.backButton.iconColor}
                onButtonClick={() => { } } 
            />
            <View style={{marginTop: 130, marginHorizontal: 35}}>
                <Description
                    Textstyle={{
                        fontFamily: "Oxygen-Regular"
                    }} 
                    text={'Boreless plan now is ready... '} 
                />
                <Description 
                    Textstyle={{
                        fontFamily: "Oxygen-Regular"
                    }}
                    text={'XXXX'} 
                />
                <Description 
                    Textstyle={{
                        fontFamily: "Oxygen-Regular"
                    }}
                    text={'XXXX'} 
                />
                <Description 
                    Textstyle={{
                        fontFamily: "Oxygen-Regular"
                    }}
                    text={'XXXX '} 
                />
            </View>
            <CmButton 
                label={'Show me the plan!'} 
                buttonStyle={{
                    backgroundColor: theme.buttonColors.primary.active,
                    marginHorizontal: 60,
                    marginTop: 167
                }} 
                buttonFocusedColor={theme.buttonColors.primary.focused} 
                buttonLabelStyle={{
                    color: theme.buttonLabelColor.white
                }} 
                onClick={() => {
                    navigation.navigate('DashBoard')
                }} />
        </ScrollView>
    );
}