import React from 'react';
import {
  View,
  Text,
  Image,
  ScrollView,
  FlatList,
  TouchableOpacity,
} from 'react-native';

const activityData = [
  {
    activity: [
      {
        activityName: 'Activity Name',
        image: '',
        id: 1,
      },

      {
        activityName: 'Activity Name',
        image: '',
        id: 2,
      },

      {
        activityName: 'Activity Name',
        image: '',
        id: 3,
      },

      {
        activityName: 'Activity Name',
        image: '',
        id: 4,
      },

      {
        activityName: 'Activity Name',
        image: '',
        id: 5,
      },

      {
        activityName: 'Activity Name',
        image: '',
        id: 6,
      },

      {
        activityName: 'Activity Name',
        image: '',
        id: 7,
      },

      {
        activityName: 'Activity Name',
        image: '',
        id: 8,
      },

      {
        activityName: 'Activity Name',
        image: '',
        id: 9,
      },
    ],
  },
  {
    activity: [
      {
        activityName: 'Game name',
        image: '',
        id: 11,
      },

      {
        activityName: 'Game name',
        image: '',
        id: 12,
      },

      {
        activityName: 'Game name',
        image: '',
        id: 13,
      },

      {
        activityName: 'Game name',
        image: '',
        id: 14,
      },

      {
        activityName: 'Game name',
        image: '',
        id: 15,
      },

      {
        activityName: 'Game name',
        image: '',
        id: 16,
      },
    ],
  },
];

const Activity = ({data, navigation}: {data: any; navigation: any}) => {
  console.log(navigation);

  const {activityName, image, id} = data;
  return (
    <TouchableOpacity
      onPress={() => {
        navigation.navigate('Activities');
      }}
      style={{
        flex: 1,
        height: 143,
        backgroundColor: '#F2F4FE',
        alignItems: 'center',
        borderRadius: 7,
        marginTop: 13,
        marginHorizontal: 6,
        justifyContent: 'center',
      }}>
      <View
        style={{
          width: 50,
          height: 50,
          backgroundColor: 'black',
          borderRadius: 25,
        }}></View>
      <Text
        style={{
          marginTop: 15,
          fontFamily: 'Oxygen-Bold',
          color: '#000000',
          fontSize: 18,
        }}>
        {activityName}
      </Text>
    </TouchableOpacity>
  );
};

const Search = ({navigation}: any) => {
  console.log('*****', navigation);
  return (
    <View
      style={{
        flex: 1,
        display: 'flex',
        justifyContent: 'space-around',
        backgroundColor: 'white',
      }}>
      <View style={{display: 'flex', alignItems: 'center', marginTop: 25}}>
        <Image source={require('../../assets/images/bo.png')}></Image>
      </View>
      <View style={{padding: 20}}>
        <Text style={{fontFamily: 'Oxygen-Bold', fontSize: 24, color: 'black'}}>
          I want to join
        </Text>
      </View>
      <FlatList
        data={activityData}
        style={{margin: 10}}
        keyExtractor={(item, index) => index.toString()}
        renderItem={({item}) => {
          return (
            <View style={{marginBottom: 10}}>
              <Text
                style={{
                  marginLeft: 10,
                  color: '#4F689C',
                  fontFamily: 'Oxygen-Regular',
                }}>
                Activity
              </Text>
              <FlatList
                data={item.activity}
                numColumns={2}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => {
                  console.log(item);
                  return <Activity data={item} navigation={navigation} />;
                }}></FlatList>
            </View>
          );
        }}></FlatList>
    </View>
  );
};

export default Search;
