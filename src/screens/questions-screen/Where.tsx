import React, {useState} from 'react';
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableHighlight,
  TouchableOpacity,
  View,
} from 'react-native';
import {CheckBox} from 'react-native-elements';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import CmButton from '../../components/CmButton';
import CustomSlider from '../../components/CustomSlider';
import Colors from '../../constants/Colors';
import {useTheme} from '../../context/ThemeProvider';

const Where = ({navigation}:any) => {
    const theme = useTheme()
  const [isSelected, setSelection] = useState(false);
  return (
    <ScrollView contentContainerStyle={{flexGrow: 1}} style={{backgroundColor: theme.backgroundColor}}>
      <View style={styles.buttonContainer}>
        <TouchableOpacity
          activeOpacity={1}
          onPress={()=>{navigation.goBack()}}
          style={{
            borderWidth: 1,
            borderColor: 'rgba(0,0,0,0.2)',
            width: 48,
            height: 48,
            backgroundColor: '#fff',
            borderRadius: 50,
            justifyContent: 'center',
            alignItems: 'center',
          }}>
          <Icon
            name="arrow-left"
            size={25}
            color="#894CED"
            style={{fontFamily: 'Light'}}
          />
        </TouchableOpacity>
        <View style={styles.textContainer}>
          <Text style={styles.textStyle}>Where</Text>
        </View>
      </View>
      <View style={styles.discriptionContainer}>
        <Text style={styles.discriptionStyle}>Some description here...</Text>
      </View>
      <View style={styles.distanceSliderContainer}>
        {/* <View style={{borderWidth: 1, borderColor: "red", marginHorizontal: 20}}>
                </View>
                <View style={{borderWidth: 2, position: "absolute", top: -8 ,backgroundColor: "red", height:20, width: 20, borderRadius: 50, marginHorizontal: 20}}></View>
                <View style={{borderWidth: 2, position: "absolute", top: -13, left: 80 ,backgroundColor: "red", height:30, width: 30, borderRadius: 50, marginHorizontal: 20}}></View>
                <View style={{borderWidth: 3, position: "absolute", top: -35, left: 92 ,borderColor: "red",  marginHorizontal: 20,  height: 45,}}></View>
                <View style={{position: "absolute", top: -80, left: 43 , backgroundColor:"red",  marginHorizontal: 20,  height: 48, width: 104, borderRadius: 25, alignItems: "center", justifyContent: "center"}}>
                    <Text style={{color: "black", fontSize: 18}}>13Km</Text>
                </View> */}
        <CustomSlider />
      </View>
      <View style={styles.checkBoxContainer}>
        <CheckBox
          checkedColor={isSelected ? '#894CED' : 'transparent'}
          activeOpacity={1}
          checked={isSelected}
          onPress={() => setSelection(pre => !isSelected)}
          uncheckedColor="#894CED"
          containerStyle={{
            backgroundColor: 'transparent',
            borderColor: 'transparent',
          }}
        />
        <Text
          style={{
            color: 'black',
            fontFamily: 'Oxygen-Bold',
            fontSize: 16,
            position: "relative",
            left: -10
          }}>
          Anywhere
        </Text>
      </View>
      <CmButton label={'Continue'} buttonStyle={{backgroundColor: theme.buttonColors.primary.active, marginHorizontal: 60, marginTop: 50}} buttonFocusedColor={''} buttonLabelStyle={{color: "white"}} onClick={() => {navigation.navigate('WhatActivies')}} />
    </ScrollView>
  );
};

export default Where;

const styles = StyleSheet.create({
    
  container: {
    flex: 1,
    backgroundColor: Colors.primaryBackground,
  },

  buttonContainer: {
    marginTop: 30,
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 25,
  },

  textContainer: {
    width: 256,
    alignItems: 'center',
  },

  textStyle: {
    color: 'black',
    fontFamily: 'Oxygen-Bold',
    fontSize: 18,
    // fontWeight: '700',
  },

  discriptionContainer: {
    marginTop: 64,
    marginHorizontal: 25,
  },

  discriptionStyle: {
    color: 'black',
    fontSize: 16,
    fontFamily: 'Oxygen-Regular',
  },

  distanceSliderContainer: {
    marginTop: 130,
  },

  checkBoxContainer: {
    padding: 0,
    marginHorizontal: 10,
    flexDirection: 'row',
    alignItems: "center"
  },

  continueButtonContainer: {
    marginTop: 60,
    marginHorizontal: 60,
    height: 48,
    // backgroundColor: "black",
    justifyContent: 'center',
    borderWidth: 1,
    borderColor: 'black',
  },

  continueButtonTitleStyle: {
    color: 'black',
    fontSize: 18,
    padding: 12,
    textAlign: 'center',
  },
});
