import React from 'react';
import { FlatList, ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ActivityDetails from '../../components/ActivityDetails';
import Colors from '../../constants/Colors';
import { useTheme } from '../../context/ThemeProvider';

const sportsActiviesData = [
    {
        activityName : "Basketball",
        isSelected: false,
        index: 1,
    },
    {
        activityName : "Football",
        isSelected: false
    },
    {
        activityName : "Rugby",
        isSelected: false
    },
    {
        activityName : "Badminton",
        isSelected: false
    },
    {
        activityName : "Futsal",
        isSelected: false
    },
    {
        activityName : "Baseball",
        isSelected: false
    },
    {
        activityName : "Yoga",
        isSelected: false
    },
    {
        activityName : "Kendo",
        isSelected: false
    },
    {
        activityName : "Pilates",
        isSelected: false
    },
    {
        activityName : "Archery",
        isSelected: false
    },
    {
        activityName : "Swimming",
        isSelected: false
    },
    {
        activityName : "Cricket",
        isSelected: false
    },
    {
        activityName : "Cricket",
        isSelected: false
    },
    {
        activityName : "Cricket",
        isSelected: false
    },
    {
        activityName : "Cricket",
        isSelected: false
    },
    {
        activityName : "Cricket",
        isSelected: false
    },
    {
        activityName : "Cricket",
        isSelected: false
    },
    {
        activityName : "Cricket",
        isSelected: false
    },
    {
        activityName : "Cricket",
        isSelected: false
    },
    {
        activityName : "Cricket",
        isSelected: false
    }
]

const WhatActivies = ({navigation}:any) => {
    const theme = useTheme()
    return (
        <ScrollView contentContainerStyle={{flexGrow: 1}} style={{backgroundColor: theme.backgroundColor}}>
            <View style={styles.buttonContainer}>
                <TouchableOpacity
                    activeOpacity={1}
                    style={{
                        borderWidth:1,
                        borderColor:'rgba(0,0,0,0.2)',
                        width:48,
                        height:48,
                        backgroundColor:'#fff',
                        borderRadius:50,
                        justifyContent: "center",
                        alignItems : "center"
                    }}
                >
                <Icon name="arrow-left" size={25} color="#894CED" style={{fontFamily:"Light"}}/>
                </TouchableOpacity>
                <View style={styles.textContainer}>
                    <Text style={styles.textStyle}>What activities?</Text>
                </View>
            </View>
            <View style={styles.discriptionContainer}>
                <Text style={styles.discriptionStyle}>Choose some activies...</Text>
            </View>
            <View style={{marginTop: 10, marginHorizontal: 25, borderBottomColor: 'rgba(242,244,254, 0.6)'}}>
                <ActivityDetails />
            </View>
            <View style={styles.continueButtonContainer}>
                <TouchableHighlight underlayColor="white" onPress={() => {navigation.navigate('Done') } }>
                    <Text style={styles.continueButtonTitleStyle}>Continue</Text>
                </TouchableHighlight>
            </View>
        </ScrollView>
    );
}

export default WhatActivies;

const styles = StyleSheet.create({
    container : {
      flex: 1,
      backgroundColor: Colors.primaryBackground,
    },

    buttonContainer : {
        marginTop: 30,
        flexDirection : 'row',
        alignItems: 'center',
        marginHorizontal: 25
    },

    textContainer: {
        width: 256,
        alignItems: 'center',
    },

    textStyle : {
        color: '#6332B3',
        fontFamily: 'Oxygen-Bold',
        fontSize: 24,
        fontWeight: "500"
    },

    discriptionContainer: {
        marginTop: 64,
        marginHorizontal: 25
    },

    discriptionStyle: {
        color: "#050B31",
        fontSize: 16,
        fontFamily: 'Oxygen-Regular'
    },

    continueButtonContainer : {
        marginTop: 60,
        marginHorizontal: 60,
        height: 48,
        backgroundColor: "#894CED",
        justifyContent: "center",
        borderWidth: 1,
        borderColor: "white"
    },

    continueButtonTitleStyle: {
        color: "white",
        fontSize: 18,
        padding: 12,
        textAlign: "center"
    },
})