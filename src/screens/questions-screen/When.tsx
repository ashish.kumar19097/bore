import React, { useEffect, useRef, useState } from 'react';
import { GestureResponderEvent, ScrollView, StyleSheet, Text, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import BackButton from '../../components/BackButton';
import CmButton from '../../components/CmButton';
import { Description } from '../../components/Description';
import Colors from '../../constants/Colors';
import { useTheme } from '../../context/ThemeProvider';


const When = ({navigation}:any) => {
    const daysGreeting = [
        {
            greetings : "Morning",
            isActive : true
        },
         
        {
            greetings : "Afternoon",
            isActive : false
        },

        {
            greetings : "Evening",
            isActive : false
        },

        {
            greetings : "Anytime",
            isActive : false
        }
    ]

    const days = [
        {
            day : "Mon",
            isSelected: false
        },
        {
            day : "Tue",
            isSelected: false
        },
        {
            day : "Wed",
            isSelected: false
        },
        {
            day : "Thu",
            isSelected: false
        },
        {
            day : "Fri",
            isSelected: false
        },
        {
            day : "Sat",
            isSelected: false
        },
        {
            day : "Sun",
            isSelected: false
        }
    ]
    const [clickedDay, setClickedDay] = useState<boolean>(false);
    const [daysData, setDaysData] = useState(days);
    const [greetingData, setGreetingData] = useState(daysGreeting)
    const theme = useTheme();


    const onClickdays = (item: String, index: number) => {
        const newDaysData = days.map((e, i) => {
            if (i == index) {
                return {
                    ...e,
                    isSelected : true
                }
            }

            return {
                ...e,
                isSelected : false
            }
        })
        setClickedDay(true);
        setDaysData(newDaysData);
    }

    const onClickGreetings = (item: String, index: number) => {
        const newGreetinsData = daysGreeting.map((e, i) => {
            console.log(index)
            if (i == index) {
                return {
                    ...e,
                    isActive : true
                }
            }

            return {
                ...e,
                isActive : false
            }
        })
        setGreetingData(newGreetinsData);
    }

    return (
        <ScrollView contentContainerStyle={{flexGrow: 1}} style={{backgroundColor: theme.backgroundColor}}>
            <View style={styles.headerContainer}>
                <BackButton 
                    buttonColor={theme.backButton.backgroundColor} 
                    iconColor={theme.backButton.iconColor} 
                    onButtonClick={() => {}} />
                <View style={styles.questionTextContainer}>
                    <Text style={{fontFamily: "Oxygen-Bold", fontSize: 18, color: theme.secondaryTextColor}}>When?</Text>
                </View>
            </View>
            <View style={styles.discriptionContainer}>
                <Description 
                    text={'Some description here...'} 
                    Textstyle={{
                        color: theme.primaryTextColor,
                        fontFamily: "Oxygen-Regular",
                        fontSize: 16
                    }} 
                />
            </View>
            <View style={styles.daysContainer}>
                {daysData.map((item, index) => {
                    return <TouchableHighlight activeOpacity={1} underlayColor={"transparent"} onPress={() => onClickdays(item.day, index)} key={index} style={[styles.activeDaysStyle, {backgroundColor: item.isSelected ? theme.ActionTextColor : "transparent"}]} >
                        <Text style={{fontFamily: "Oxygen-Regular", color: item.isSelected ? theme.buttonLabelColor.white : theme.buttonLabelColor.black, fontSize: 16}}>{item.day}</Text>
                    </TouchableHighlight>
                })}
            </View>
            {clickedDay ? <>
                {greetingData.map((item, index) => {
                    // return <View key={index} style={{ width: "100%", height: 36, marginTop: 10 }}>
                    //     <TouchableHighlight underlayColor="transparent" onPress={() => onClickGreetings(item.greetings, index)} style={{ backgroundColor: item.isActive ? theme.ActionTextColor : "transparent", flex: 1, flexDirection: "row", width: "100%", alignItems: "center", justifyContent: "center", borderRadius: 20}}>
                    //         <Text style={{fontFamily: "Oxygen-Regular", color: theme.primaryTextColor, fontSize: 14}}>{item.greetings}</Text>
                    //     </TouchableHighlight>
                    // </View>;
                    return <CmButton 
                                key={index}
                                label={item.greetings} 
                                buttonStyle={{
                                    backgroundColor: item.isActive ? theme.buttonColors.primary.active : "transparent",
                                    borderRadius: 20,
                                    marginHorizontal: 73, 
                                    marginBottom: 10
                                }} 
                                buttonFocusedColor={"transparent"} 
                                buttonLabelStyle={{
                                    fontFamily: "Oxygen-Regular",
                                    color: item.isActive ? theme.buttonLabelColor.white : theme.buttonLabelColor.black
                                }} 
                                onClick={()=>onClickGreetings(item.greetings, index)}  
                            />
                })}
                <CmButton 
                    label={'Delete this date'}
                    buttonStyle={{
                        marginHorizontal: 60,
                        marginTop: 10,
                        borderWidth: 2,
                        borderColor: theme.buttonColors.outline.active
                    }} 
                    buttonFocusedColor={theme.buttonColors.outline.focused} 
                    buttonLabelStyle={{
                        color: theme.buttonLabelColor.purple
                    }} 
                    onClick={() => {}} 
                />
                <CmButton 
                    label={'Continue'}
                    buttonStyle={{
                        marginHorizontal: 60,
                        marginTop: 40,
                        backgroundColor: theme.buttonColors.outline.active
                    }} 
                    buttonFocusedColor={theme.buttonColors.outline.focused} 
                    buttonLabelStyle={{
                        color: theme.buttonLabelColor.white
                    }} 
                    onClick={() => {navigation.navigate('Where')}} 
                />
            </>: <></>}
        </ScrollView>
    );
}

export default When;

const styles = StyleSheet.create({
    
    headerContainer : {
        marginTop: 57,
        flexDirection : 'row',
        alignItems: 'center',
        marginHorizontal: 25
    },

    questionTextContainer: {
        width: 256,
        alignItems: 'center',
    },

    textStyle : {
        color: 'white',
        fontFamily: 'Oxygen-Regular',
        fontSize: 24,
        fontWeight: "500"
    },

    discriptionContainer: {
        marginTop: 64,
        marginHorizontal: 25
    },

    discriptionStyle: {
        color: "white",
        fontSize: 16,
        fontFamily: 'Oxygen-Regular'
    },

    daysContainer : {
        marginTop: 28,
        marginHorizontal: 20,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-evenly",
        marginBottom: 20
    },

    daysStyle : {
        color: "white",
        fontSize: 35
    },

    activeDaysStyle : {
        borderColor:'rgba(0,0,0,0.2)',
        width:48,
        height:48,
        borderRadius:50,
        justifyContent: "center",
        alignItems : "center",
    },

})
