import React, { useState } from 'react';
import { ScrollView, StyleSheet, TextInput, TouchableHighlight, TouchableOpacity, View } from 'react-native';
import Colors from '../../constants/Colors';
// import Icon from 'react-native-vector-icons/FontAwesome5';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import { Text } from 'react-native-elements';
import { useTheme } from '../../context/ThemeProvider';
import CmTextInput from '../../components/CmTextInput';
import BackButton from '../../components/BackButton';
import CmButton from '../../components/CmButton';


const Who = ({navigation}:any) => {

    const [nameFieldChange, setNameFieldChange] = useState(false)
    const theme = useTheme()
    const nameFieldValidator = (name : string) => {
        if (name.length >= 1) {
            setNameFieldChange(true);
        } else {
            setNameFieldChange(false)
        }
    }

    return (
        <ScrollView contentContainerStyle={{flexGrow: 1}} style={{backgroundColor: theme.backgroundColor}}>
            <View style={styles.backButtonContainer}>
                <BackButton 
                    buttonColor={theme.backButton.backgroundColor} 
                    iconColor={theme.backButton.iconColor} 
                    onButtonClick={() => {navigation.goBack()}} />
                <View style={styles.questiontTextContainer}>
                    <Text style={{fontFamily: "Oxygen-Bold", fontSize: 18, color: theme.secondaryTextColor}}>Who are you?</Text>
                </View>
            </View>
            <View style={{marginHorizontal: 25, marginTop: 109}}>
                <Text style={{color: theme.secondaryTextColor, fontFamily: "Oxygen-Bold", fontSize: 18}}>Name</Text>
                <View style={{marginTop: 10}}>
                    <CmTextInput 
                        placeHolder={"Name"}
                        placeHolderTextColor={theme.placeHolderTextColor} 
                        nameFieldValidator={nameFieldValidator}
                    />
                </View>
                {/* <Text style={{color: "white", fontSize: 16}}>Please input your name</Text> */}
            </View>
            {nameFieldChange ? <View style={styles.continueButtonContainer}>
                <CmButton 
                    label={'Continue'} 
                    buttonStyle={{
                        backgroundColor: theme.buttonColors.primary.active
                    }} 
                    buttonFocusedColor={theme.buttonColors.primary.focused} 
                    buttonLabelStyle={{
                        color: theme.buttonColors.primary.labelColor
                    }} 
                    onClick={() => {
                        navigation.navigate('When')
                    }} />
            </View> : <View></View>}
        </ScrollView>
    );
}

const styles = StyleSheet.create({

    container : {
      flex: 1,
      backgroundColor: Colors.primaryBackground,
    },

    backButtonContainer : {
        marginTop: 57,
        flexDirection : 'row',
        alignItems: 'center',
        marginHorizontal: 25
    },

    questiontTextContainer: {
        width: 256,
        alignItems: 'center',
    },

    continueButtonContainer : {
        marginTop: 230,
        marginHorizontal: 60,
        backgroundColor: "white",
        justifyContent: "center"
    },
});

export default Who;