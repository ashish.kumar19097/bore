import * as React from 'react';
import {FlatList, Text, View} from 'react-native';
import {Avatar, Card} from 'react-native-paper';
import ActivityData from '../constants/eventDetails';
import { useTheme } from '../context/ThemeProvider';

const Chat = ({navigation}:any) => {
  var date = new Date().getDate();
  const theme = useTheme();
  const LeftContent = (props: any) => (
    <Text
      style={{
        paddingRight: 10,
        fontFamily: 'Oxygen',
        fontSize: 14,
        color: '#4F689C',
      }}>
      9:45
    </Text>
  );
  return (
    <View style = {{backgroundColor: theme.backgroundColor ,height:'100%'}}>
      <FlatList
        style={{zIndex: 1}}
        data={ActivityData}
        renderItem={({item}) => {
          return (
            <Card
              onPress={() => {
                navigation.navigate('ChatRoom',{eventName:item.eventName})
              }}
              style={{
                marginBottom: 5,
                marginTop: 5,
                marginLeft: 5,
                marginRight: 5,
              }}>
              <Card.Title
                title={item.eventName}
                titleStyle={{
                  fontFamily: 'Oxygen',
                  fontWeight: 'bold',
                  fontSize: 16,
                  color: '#050B31',
                }}
                subtitle="Card Subtitle"
                subtitleStyle={{
                  fontFamily: 'Oxygen',
                  fontSize: 14,
                  color: '#000000',
                }}
                right={LeftContent}
              />
            </Card>
          );
        }}
      />
    </View>
  );
};

export default Chat;
