import React, {useCallback, useEffect, useState} from 'react';
import {ScrollView, View} from 'react-native';
import {Composer, GiftedChat} from 'react-native-gifted-chat';
import {Text, TextInput} from 'react-native-paper';
import BackButton from '../components/BackButton';
import {useTheme} from '../context/ThemeProvider';
import {LogBox} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';

function ChatRoom({navigation, route}: any) {
  const [messages, setMessages] = useState<any[]>([]);
  const [keyBoard, setKeyBoard] = useState(true);
  const theme = useTheme();
  useEffect(() => {
    setMessages([
      {
        _id: 1,
        text: 'Hello developer',
        createdAt: new Date(),
        user: {
          _id: 2,
          name: 'React Native',
          avatar: 'https://placeimg.com/140/140/any',
        },
      },
    ]);
  }, []);

  const onSend = useCallback((messages = []) => {
    setMessages(previousMessages =>
      GiftedChat.append(previousMessages, messages),
    );
  }, []);
  return (
    <View
      style={{
        flex: 1,
        backgroundColor: 'white',
      }}>
      <View
        style={{
          padding: 10,
          paddingTop: 30,
          flexDirection: 'row',
          justifyContent: 'space-between',

          //alignItems: 'flex-start',
        }}>
        <BackButton
          onButtonClick={() => {
            LogBox.ignoreLogs(['EventEmitter.removeListener']);
            navigation.goBack();
          }}
          buttonColor={theme.backgroundColor}
          iconColor={theme.backButton.iconColor}
        />
        <Text
          style={{
            flex: 1,
            textAlign: 'center',
            right: 10,
            alignSelf: 'center',
            fontSize: 22,
            fontWeight: 'bold',
            fontFamily: 'Oxygen',
            color: theme.primaryTextColor,
          }}>
          {route.params.eventName}
        </Text>
      </View>
      <GiftedChat
        messages={messages}
        showAvatarForEveryMessage={true}
        alignTop={true}
        onSend={messages => onSend(messages)}
        user={{
          _id: 1,
        }}
        children={
          <Icon
            name="send-circle"
            size={40}
            style={{
              fontFamily: 'Light',
              color: theme.buttonColors.primary.active,
              marginRight: 10,
            }}
          />
        }
      />
    </View>
  );
}

export default ChatRoom;
