import {View, Text, ScrollView} from 'react-native';
import React, {useState} from 'react';
import {useTheme} from '../../context/ThemeProvider';
import BackButton from '../../components/BackButton';
import CmButton from '../../components/CmButton';
import {Icon, Slider} from 'react-native-elements';
import ProfileStatus from '../../components/ProfileStatus';

const PaymentMethod = ({navigation}:any) => {
  const theme = useTheme();
  const [cardDisplay, setCardDisplay] = useState(false);

  return (
    <ScrollView
      automaticallyAdjustContentInsets={true}
      contentContainerStyle={{flexGrow: 1, backgroundColor: 'white'}}>
      <View
        style={{
          padding: 10,
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          marginTop: 10,
        }}>
        <BackButton
          onButtonClick={() => {navigation.goBack()}}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
          buttonStyle={{backgroundColor: '#F2F4FE'}}
        />
        <Text
          style={{
            fontFamily: 'Oxygen-Bold',
            fontSize: 18,
            color: '#050B31',
            marginLeft: '20%',
          }}>
          Payment Method
        </Text>
      </View>
      <View>
        <View
          style={{
            display: 'flex',
            backgroundColor: '#F2F4FE',
            height: 180,
            marginHorizontal: 25,
            borderRadius: 10,
          }}>
          <Text
            style={{
              fontFamily: 'Oxygen-Bold',
              fontSize: 24,
              color: 'black',
              alignSelf: 'flex-end',
              padding: 15,
            }}>
            Visa
          </Text>
          <View style={{flexDirection: 'row', marginHorizontal: 20}}>
            <Text
              style={{
                fontFamily: 'Oxygen-Bold',
                fontSize: 18,
                color: 'black',
                letterSpacing: 10,
              }}>
              ....
            </Text>
            <Text
              style={{
                fontFamily: 'Oxygen-Bold',
                fontSize: 18,
                color: 'black',
              }}>
              1234
            </Text>
          </View>
          <Text
            style={{
              fontFamily: 'Oxygen-Regular',
              fontSize: 14,
              color: '#4F689C',
              marginHorizontal: 20,
              marginTop: 15,
            }}>
            Expiration date
          </Text>
          <Text
            style={{
              fontFamily: 'Oxygen-Bold',
              fontSize: 18,
              color: 'black',
              marginHorizontal: 20,
            }}>
            01/2025
          </Text>
        </View>
        <CmButton
          label={'Delete this card'}
          buttonStyle={{
            borderColor: theme.buttonColors.outline.active,
            borderWidth: 2,
            marginHorizontal: 60,
            marginTop: 40,
          }}
          buttonFocusedColor={theme.buttonColors.outline.focused}
          buttonLabelStyle={{
            fontFamily: 'Oxygen-Bold',
            fontSize: 16,
            color: theme.ActionTextColor,
          }}
          onClick={() => {}}
        />
        <CmButton
          label={'Edit'}
          buttonStyle={{
            backgroundColor: theme.buttonColors.primary.active,
            marginHorizontal: 60,
            marginTop: 20,
          }}
          buttonFocusedColor={theme.buttonColors.primary.focused}
          buttonLabelStyle={{
            color: 'white',
          }}
          onClick={() => {}}
        />
      </View>
    </ScrollView>
  );
};

export default PaymentMethod;
