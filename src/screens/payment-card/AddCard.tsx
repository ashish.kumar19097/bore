import {View, Text, ScrollView, TextInput} from 'react-native';
import React, {useState} from 'react';
import {useTheme} from '../../context/ThemeProvider';
import BackButton from '../../components/BackButton';
import CmTextInput from '../../components/CmTextInput';
import SelectDropdown from 'react-native-select-dropdown';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import ProfileStatus from '../../components/ProfileStatus';
import CmButton from '../../components/CmButton';

const monthNumber = ['01', '02', '03', '04', '05', '06'];
const year = ['2022', '2023', '2024', '2025', '2026', '2027'];

const AddCard = ({navigation, route}: any) => {
  const theme = useTheme();
  const [cardDisplay, setCardDisplay] = useState(false);
  const [cardNumber, setCardNumber] = useState();
  const [cardExpiryMonth, setCardExpiryMonth] = useState();
  const [cardExpiryYear, setCardExpiryYear] = useState();
  const [cardCvv, setCardCvv] = useState();
  const [cardHolder, setCardHolder] = useState();
  !!route?console.log('****',route):console.log('111111',route)
  return (
    <ScrollView
      automaticallyAdjustContentInsets={true}
      contentContainerStyle={{
        flexGrow: 1,
        backgroundColor: theme.backgroundColor,
      }}>
      <View
        style={{
          padding: 20,
          display: 'flex',
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <BackButton
          onButtonClick={() => {
            cardDisplay ? setCardDisplay(false) : navigation.goBack();
          }}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
        />
        <Text
          style={{fontFamily: 'Oxygen-Bold', fontSize: 18, color: '#050B31'}}>
         {!!route.params ? 'I want to be an event owner' : "Add Payment Card"} 
        </Text>
        <BackButton
          onButtonClick={() => {
            navigation.goBack();
          }}
          buttonColor={theme.backButton.backgroundColor}
          iconColor={theme.backButton.iconColor}
          icon="close"
        />
      </View>
      {cardDisplay ? (
        <>
          <View style={{padding: 10, marginHorizontal: 10}}>
            <Text
              style={{
                fontFamily: 'Oxygen-Bold',
                fontSize: 18,
                color: '#050B31',
                marginBottom: 10,
              }}>
              Card Number
            </Text>
            <CmTextInput
              placeHolder="Enter your card number"
              placeHolderTextColor={'#4F689C'}
            />
          </View>
          <View
            style={{
              display: 'flex',
              flexDirection: 'row',
              justifyContent: 'space-between',
              padding: 10,
              marginHorizontal: 10,
            }}>
            <View>
              <Text
                style={{
                  fontFamily: 'Oxygen-Bold',
                  fontSize: 18,
                  color: '#050B31',
                  marginBottom: 10,
                }}>
                Expiration month
              </Text>
              <SelectDropdown
                buttonStyle={{width: 150, backgroundColor: 'white'}}
                renderDropdownIcon={() => (
                  <Icon
                    name="menu-down-outline"
                    size={25}
                    style={{fontFamily: 'Light'}}
                  />
                )}
                defaultValueByIndex={0}
                data={monthNumber}
                onSelect={(selectedItem, index) => {
                  console.log(selectedItem, index);
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                  // text represented after item is selected
                  // if data array is an array of objects then return selectedItem.property to render after item is selected
                  return selectedItem;
                }}
                rowTextForSelection={(item, index) => {
                  // text represented for each item in dropdown
                  // if data array is an array of objects then return item.property to represent item in dropdown
                  return item;
                }}
              />
            </View>
            <View>
              <Text
                style={{
                  fontFamily: 'Oxygen-Bold',
                  fontSize: 18,
                  color: '#050B31',
                  marginBottom: 10,
                }}>
                Expiration year
              </Text>
              <SelectDropdown
                buttonStyle={{width: 150, backgroundColor: 'white'}}
                renderDropdownIcon={() => (
                  <Icon
                    name="menu-down-outline"
                    size={25}
                    style={{fontFamily: 'Light'}}
                  />
                )}
                defaultValueByIndex={0}
                data={year}
                onSelect={(selectedItem, index) => {
                  console.log(selectedItem, index);
                }}
                buttonTextAfterSelection={(selectedItem, index) => {
                  // text represented after item is selected
                  // if data array is an array of objects then return selectedItem.property to render after item is selected
                  return selectedItem;
                }}
                rowTextForSelection={(item, index) => {
                  // text represented for each item in dropdown
                  // if data array is an array of objects then return item.property to represent item in dropdown
                  return item;
                }}
              />
            </View>
          </View>
          <View style={{padding: 20}}>
            <Text
              style={{
                fontFamily: 'Oxygen-Bold',
                fontSize: 18,
                color: '#050B31',
                marginBottom: 10,
              }}>
              CVV
            </Text>
            <TextInput
              secureTextEntry={true}
              style={{
                backgroundColor: 'white',
                width: 150,
                height: 50,
                paddingHorizontal: 20,
                fontSize: 24,
              }}></TextInput>
          </View>
          <View style={{padding: 10, marginHorizontal: 10}}>
            <Text
              style={{
                fontFamily: 'Oxygen-Bold',
                fontSize: 18,
                color: '#050B31',
                marginBottom: 10,
              }}>
              Name
            </Text>
            <CmTextInput
              placeHolder="Enter your name"
              placeHolderTextColor={'#4F689C'}
            />
          </View>
          <CmButton
            label={'Save'}
            buttonStyle={{
              backgroundColor: theme.buttonColors.primary.active,
              marginHorizontal: 60,
              marginTop: 20,
            }}
            buttonFocusedColor={theme.buttonColors.primary.focused}
            buttonLabelStyle={{
              color: 'white',
            }}
            onClick={() => {
              navigation.navigate('Profile', {verified: 'Verified'});
            }}
          />
        </>
      ) : (
        <View>
          <Text
            style={{
              marginHorizontal: 20,
              fontFamily: 'Oxygen-Bold',
              fontSize: 16,
              color: 'black',
              marginVertical: 3,
            }}>
            Some description
          </Text>
          <Text
            style={{
              marginHorizontal: 20,
              fontFamily: 'Oxygen-Bold',
              fontSize: 16,
              color: 'black',
              marginVertical: 3,
            }}>
            blslsldlafalskfaksf
          </Text>
          <Text
            style={{
              marginHorizontal: 20,
              marginVertical: 3,
              fontFamily: 'Oxygen-Bold',
              fontSize: 16,
              color: 'black',
            }}>
            blablablablablabla
          </Text>
          <Text
            style={{
              marginHorizontal: 20,
              marginVertical: 20,
              fontFamily: 'Oxygen-Bold',
              fontSize: 25,
              color: 'black',
              letterSpacing: 1.5,
              fontWeight: '700',
            }}>
            What's next?
          </Text>
          <ProfileStatus
            label1={!!route.params ? 'Add payment card' : "Create account"}
            lable2={!!route.params ? 'Verify Identity' : "Add payment card"}
            lable3={!!route.params ? 'Add cash' : undefined}
            goClick={() => {
              {!!route.params ? navigation.navigate('Profile',{host:true}) : setCardDisplay(true)}
            }}
          />
        </View>
      )}
    </ScrollView>
  );
};

export default AddCard;
