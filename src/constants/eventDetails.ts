
const ActivityData = [
  
  {
    id: 1,
    eventName: 'BasketBall',
    image: require('../../assets/images/Small.png'),
    level: 'Advanced',
    totalMember: '12',
    currentEnroll: '5',
    date: '28-12-2021',
    place: 'japan',
    distance: '17km',
    charges: '500',
  },
  {
    id: 2,
    eventName: 'Cricket',
    image: require('../../assets/images/Small.png'),
    level: 'Advanced',
    totalMember: '12',
    currentEnroll: '5',
    date: '25-12-2021',
    place: 'japan',
    distance: '13km',
    charges: '200',
  },
  {
    id: 3,
    eventName: 'Dancing',
    image: require('../../assets/images/Small.png'),
    level: 'Advanced',
    totalMember: '12',
    currentEnroll: '5',
    date: '26-12-2021',
    place: 'japan',
    distance: '20km',
    charges: '100',
  },
  {
    id: 4,
    eventName: 'Farming',
    image: require('../../assets/images/Small.png'),
    level: 'Advanced',
    totalMember: '12',
    currentEnroll: '5',
    date: '27-12-2021',
    place: 'japan',
    distance: '15km',
    charges: '600',
  },
  
  
];
export default ActivityData;