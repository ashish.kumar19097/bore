import { BottomNavigation } from "react-native-paper"

export const DefaultTheme = {
    backgroundColor : "#F2F4FE",
    primaryTextColor : "#000000",
    placeHolderTextColor : "#4F689C",
    ActionTextColor : "#894CED",
    welcomeTextColor : "#6332B3",
    secondaryTextColor : "#050B31",
    buttonColors : {
        primary : {
            active : "#894CED",
            inActive : "#AFAFAF",
            focused : "#6332B3",
            labelColor: "#fff"
        },

        secondary : {
            active : "#F2F4FE",
            focused : "#6332B3",
            inActive : "#AFAFAF"
        },

        outline : {
            active : "#894CED",
            focused : "#6332B3",
            inActive : "#AFAFAF"
        },

    },

    buttonLabelColor : {
        white : "#fff",
        black : "#000000",
        purple : "#894CED"
    },

    backButton : {
        backgroundColor : "#fff",
        iconColor : "#894CED"
    },

    bottomNavigation : {
        backgroundColor : "#fff",
        activeIconColor : "#894CED",
        inactiveIconColor : "#050B31"
    }

}

export const DarkTheme = {
    backgroundColor : "#050B31",
    primaryTextColor : "#000000",
    buttonColors : {
        primary : {
            active : "#894CED",
            inActive : "#AFAFAF",
            focused : "#6332B3"
        },

        secondary : {
            active : "#F2F4FE",
            focused : "#6332B3",
            inActive : "#AFAFAF"
        },

        outline : {
            active : "#894CED",
            focused : "#6332B3",
            inActive : "#AFAFAF"
        }
    }
}