import React from "react";

const Colors = {
    primaryBackground: "#050B31",
    secondaryBackground: "#F2F4FE",
    subtile: "#4F689C",
    secondaryColor: "#6332B3",
    activeButton: "#B143BA",
    buttonClicked: "#7B1288",
    accentColor: "#F6D573",
    primaryText: "#000000",
    
};

export default Colors;