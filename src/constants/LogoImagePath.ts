const LogoImagePath = {
    GreenColor : {
        bigHalfLetter : require('../../assets/images/greenBigHalfLetter.png'),
        smallHalfLetter : require('../../assets/images/greenSmallHalfLetter.png'),
        paragraphWords : require('../../assets/images/greenParaWords.png')
    },

    PurpleColor : {
        bigHalfLetter : require('../../assets/images/purpleBigHalfLetter.png'),
        smallHalfLetter : require('../../assets/images/purpleSmallHalfLetter.png'),
        // paragraphWords : require('')
    }
}

export default LogoImagePath;
